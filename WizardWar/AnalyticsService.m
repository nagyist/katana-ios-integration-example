//
//  AnalyticsService.m
//  WizardWar
//
//  Created by Sean Hess on 7/16/13.
//  Copyright (c) 2013 The LAB. All rights reserved.
//

#import "AnalyticsService.h"
#import "TestFlight.h"

@implementation AnalyticsService

+ (AnalyticsService *)shared {
    static AnalyticsService *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[AnalyticsService alloc] init];
        // Initalize Katana API
        instance.kApi = [[kAPI alloc] initClientID:1007 applicationID:5069 password:@"085b2c24fc47dca5aef0c766a48d75d7"];
    });
    return instance;
}

+(void)didFinishLaunching:(NSDictionary *)launchOptions {
}

+(void)event:(NSString *)name {
    [TestFlight passCheckpoint:name];
}

@end
