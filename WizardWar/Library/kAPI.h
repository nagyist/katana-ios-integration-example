//
//  kAPI.h
//  Katana
//
//  Created by Jon Hovland on 3/17/13.
//
//

#import <Foundation/Foundation.h>
@class KatanaReachability;
@interface kAPI : NSObject{
    KatanaReachability *internetReach;
    
    int _clientID;
    int _appID;
    NSString * _password;
    
    NSString * _filename;
    
    NSString * _theAccountId;
    NSString * _theCharacterId;
    long _theShardID;
    NSString * _thePlatform;
    long _theAreaId;
    NSString * _theAreaName;
    
    
    NSTimer *_myTimer;
}

/*
 * This is the API+EVENT BASE for the batch handler
 */
#define API_BASE  @"http://batch.ninjametrics.com"
#define EVENT_BASE  @"/events"

//#define API_BASE  @"https://dev2api.ninjametrics.com"
//#define EVENT_BASE  @"/event"
#define API_LIBRARY_USER_AGENT @"iOS_1.0"
#define HEARTBEAT_INTERVAL 10 //float in seconds
#define FILE_NAME @"katana"

//For the batch handler, update this limit to a number greater than 1 (say 20)
#define CACHE_OBJECT_LIMIT 10

#define kAPP_ID @"APP_ID"
#define kAPP_PWD @"APP_PWD"
#define kCLIENT_ID @"CLIENT_ID"

#pragma mark - KDataKey stuff


#define kACCOUNT_COUNTRY @"account_country"
#define kACCOUNT_CURRENCY_BALANCE @"account_currency_balance"
#define kACCOUNT_DATE_OF_BIRTH @"account_dob"
#define kACCOUNT_GENDER @"account_gender"
#define kACCOUNT_ID @"account_id"
#define kACCOUNT_LANGUAGE @"account_lang"
#define kACCOUNT_STATUS @"account_status"
#define kACCOUNT_SUBSCRIPTION_EXPIRES @"account_sub_expires_timestamp"
#define kACCOUNT_SUBSCRIPTION_TYPE @"account_sub_type"
#define kACCOUNT_SUBSCRIPTION_VALUE @"account_sub_value"
#define kAREA_ID @"area_id"
#define kCHALLENGE_ID @"challenge_id"
#define kCHALLENGE_STATUS @"challenge_status"
#define kCHALLENGE_TYPE @"challenge_type"
#define kCHARACTER_CLASS @"character_class"
#define kCHARACTER_GENDER @"character_gender"
#define kCHARACTER_ID @"character_id"
#define kCHARACTER_LEVEL @"character_lvl"
#define kCHARACTER_NAME @"character_name"
#define kCHARACTER_RACE @"character_race"
#define kCHARACTER_SUB_CLASS @"character_sub_class"
#define kCURRENCY_TYPE @"currency_type"
#define kCURRENCY_VALUE @"currency_value"
#define kCURRENCY_VIRTUAL_COUNT @"virtual_currency_count"
#define kCURRENCY_VIRTUAL_EXCHANGE_RATE @"virtual_currency_ex_rate"
#define kCURRENCY_VIRTUAL_LABEL @"virtual_currency_label"
#define kFLAG_FARMER @"farmer_flag"
#define kFLAG_GROUPED @"grouped_flag"
#define kFLAG_INTEGRITY @"integrity_flag"
#define kFLAG_LEADER @"leader_flag"
#define kFLAG_WIN @"win_flag"
#define kGROUP_ID @"group_id"
#define kGUILD_EXIT_REASON @"guild_exit_reason"
#define kGUILD_ID @"guild_id"
#define kITEM_COUNT @"item_count"
#define kITEM_ID @"item_id"
#define kITEM_NAME @"item_name"
#define kITEM_PRICE @"item_price"
#define kITEM_SEGMENT @"item_segment"
#define kITEM_TYPE @"item_type"
#define kITEM_VALUE @"item_value"
#define kMAP_REAL_ADDRESS @"map_real_address"
#define kMAP_REAL_CITY @"map_real_city"
#define kMAP_REAL_COUNTRY @"map_real_country"
#define kMAP_REAL_POSTAL_CODE @"map_real_postal_code"
#define kMAP_REAL_STATE @"map_real_state"
#define kMESSAGE_CHANNEL_LABEL @"message_ch_label"
#define kMESSAGE_CHARACTER_COUNT @"message_char_count"
#define kMESSAGE_DESCRIPTION @"message_desc"
#define kNPC_EVENT_NAME @"npc_event_name"
#define kNPC_EVENT_TYPE @"npc_event_type"
#define kNPC_ID @"npc_id"
#define kNPC_LEVEL_MAX @"npc_level_max"
#define kNPC_LEVEL_MIN @"npc_level_min"
#define kNPC_NAME @"npc_name"
#define kNPC_TOUGHNESS @"npc_toughness"
#define kNPC_TOUGHNESS_ENUM @"npc_toughness_enum"
#define kNPC_TYPE @"npc_type"
#define kOGI_CATEGORY @"ogi_category"
#define kOGI_LOCATION @"ogi_location"
#define kOGI_RECEIVER_ACTION @"ogi_receiver_action"
#define kPLATFORM @"platform"
#define kPLAYER_DEATH_TYPE @"death_type"
#define kPLAYER_XP_AMOUNT @"xp_amount"
#define kPOSITION_X @"position_x"
#define kPOSITION_Y @"position_y"
#define kPOSITION_Z @"position_z"
#define kRATING_USER_GENERATED_CONTENT @"ugc_rating"
#define kRECEIVER_ACCOUNT_ID @"receiver_account_id"
#define kRECEIVER_CHARACTER_ID @"receiver_character_id"
#define kRECEIVER_CHARACTER_LEVEL @"receiver_character_level"
#define kRECRUITMENT_OUTCOME @"recruitment_outcome"
#define kRECRUITMENT_TYPE @"recruitment_type"
#define kSENDER_ACCOUNT_ID @"sender_account_id"
#define kSENDER_CHARACTER_ID @"sender_character_id"
#define kSENDER_CHARACTER_LEVEL @"sender_character_level"
#define kSHARD_DESCRIPTION @"shard_desc"
#define kSHARD_ID @"shard_id"
#define kSOCIAL_EVENT_NAME @"social_event_name"
#define kSOCIAL_EVENT_TYPE @"social_event_type"
#define kSTORE_DESCRIPTION @"store_desc"
#define kSTORE_ID @"store_id"
#define kTRANSACTION_DESCRIPTION @"transaction_desc"
#define kTRANSACTION_TYPE @"transaction_type"
#define kTYPE @"type"
#define kFARMER_TYPE @"farmer_type"
#define kINTEGRITY_TYPE @"integrity_type"
#define kCUSTOM_ID @"custom_id"
#define kCUSTOM_EVENT_TYPE @"custom_event_type"
#define kCUSTOM_EVENT_VALUE @"custom_event_value"
#define kOLD_ACCOUNT_ID @"old_account_id"
#define kPOINT_TYPE @"point_type"
#define kREWARD_TYPE @"reward_type"
#define kREWARD_ID @"reward_id"
#define kREWARD_COUNT @"reward_count"
#define kAREA_NAME @"area_name"
#define kMAP_ID @"map_id"
#define kMAP_LABEL @"map_label"
#define kMIN_X @"min_x"
#define kMIN_Y @"min_y"
#define kMIN_Z @"min_z"
#define kMAX_X @"max_x"
#define kMAX_Y @"max_y"
#define kMAX_Z @"max_z"
#define kMOB_DESC @"mob_desc"
//#define kMOB_KILL_TIMESTAMP @"mob_kill_timestamp"
#define kCRM_ACTION @"crm_action"
#define kCRM_ACTION_TYPE @"crm_action_type"
#define kCRM_ACTION_TIMESTAMP @"crm_action_timestamp"
#define kCRM_FULFILLED_STATUS @"crm_fulfilled_status"
#define kCRM_FULFILLED_TIMESTAMP @"crm_fulfilled_timestamp"
#define kCUSTOMER_SERVICE_ACTION @"customer_service_action"
#define kCUSTOMER_SERVICE_ACTION_TYPE @"customer_service_action_type"
#define kCUSTOM_ACTION @"custom_action"
#define kCUSTOM_ACTION_TYPE @"custom_action_type"
#define kCUSTOM_ACTION_VALUE @"custom_action_value"
#define kPRODUCTIVITY_TYPE @"productivity_type"
#define kRESOURCE_TYPE @"resource_type"
#define kRESOURCE_ID @"resource_id"
#define kRESOURCE_COUNT @"resource_count"
#define kAD_CLICK_TIMESTAMP @"ad_click_timestamp"
#define kAD_TAG_NAME @"ad_tag_name"
#define kAD_ACTION_TIMESTAMP @"ad_action_timestamp"
#define kAD_ACTION_DESC @"ad_action_desc"
#define kECONOMIC_TYPE @"economic_type"
#define kECONOMIC_VALUE @"economic_value"
#define kTRAFFIC_SOURCE @"traffic_source"
#define kTRAFFIC_SOURCE_TYPE @"traffic_source_type"


#pragma mark - KApiCode stuff


#define cLOGIN 1
#define cLOGOUT 2
#define cCHANGE_SUBSCRIPTION 3
#define cGROUP_ENTER 4
#define cGROUP_EXIT 5
#define cSOCIAL 6
#define cOFF_GAME_INTERACTION_SEND 7
#define cOFF_GAME_INTERACTION_RECEIVE 8
#define cACCOUNT_CREATE 9
#define cACCOUNT_DELETE 10
#define cCHARACTER_CREATE 11
#define cCHARACTER_DELETE 12
#define cSHARD_INIT 13
#define cVIRTUAL_CURRENCY_SET 14
#define cITEM_INIT 15
#define cITEM_USED 16
#define cITEM_TRANSACTION 17
#define cCURRENCY_TRANSACTION 18
#define cITEM_INIT_UGC_RATING 19
#define cMESSAGE 20
#define cCOMBAT_BEGIN 21
#define cCOMBAT_END 22
#define cKILL 23
#define cPLAYER_XP 24
#define cPVP_DUEL 25
#define cPLAYER_DEATH 26
#define cLEVEL_BEGIN 27
#define cLEVEL_END 28
#define cAREA_ENTER 29
#define cAREA_EXIT 30
#define cNPC_INTERACTION 31
#define cCHALLENGE 32
#define cRECRUITMENT_SEND 33
#define cRECRUITMENT_RECEIVE 34
#define cSTORE_CREATE 35
#define cSTORE_DELETE 36
#define cSTORE_BUY_ITEM 37
#define cSTORE_LOGIN 38
#define cSTORE_LOGOUT 39
#define cSTORE_CART_ADD 40
#define cGUILD_ENTER 41
#define cGUILD_EXIT 42
#define cFRIEND_ADD 43
#define cFRIEND_DELETE 44
#define cGUILD_LEADER 45
#define cUGC_COPY 46
#define cFARMER 47
#define cINTEGRITY            48
#define cREWARD               49
#define cAREA_DIM             50
#define cMOB                  51
#define cCRM_ACTION           52
#define cCUSTOMER_SERVICE_ACTION   53
#define cCUSTOM_SLOT          54
#define cPRODUCTIVITY         55
#define cRESOURCE             56
#define cAD_CLICK   57
#define cECONOMIC             58
#define cTRAFFIC_SOURCE       59

#pragma mark - variables



#pragma mark - methods



-(id) initClientID: (int)clientID applicationID:(int) applicationID password:(NSString *) password;

- (void) loadAccountCreate:(NSDate *) theDate
               theAccountId:(NSString *) theAccountId
             theAccountType:(NSString *) theAccountType
                 theAccLang:(NSString *) theAccLang
              theAccCountry: (NSString *) theAccCountry
               theAccGender: (NSString *) theAccGender
          theAccDateOfBirth: (NSDate *) theAccDateOfBirth
          theAccCurrencyBal: (double) theAccCurrencyBal
                thePlatform:(NSString *) thePlatform;

-(void) loadAccountDelete:(NSDate *) theDate
              theAccountId:(NSString *) theAccountId
              theChurnType:(NSString *) theChurnType;

- (void) logLogin:(NSDate *) theDate
     theAccountId:(NSString *) theAccountId
   theCharacterId:(NSString *) theCharacterId
       theShardId:(long) theShardId
      thePlatform:(NSString *) thePlatform
        theAreaId:(long) theAreaId
      theAreaName:(NSString *)theAreaName;

- (void) logLogout:(NSDate *) theDate
      theAccountId:(NSString *) theAccountId
    theCharacterId:(NSString *) theCharacterId;

- (void) loadItem:(NSDate *) theDate
        theItemId:(long)theItemId
      theItemName:(NSString *) theItemName
      theItemType:(NSString *) theItemType
   theItemSegment:(NSString *) theItemSegment
  createAccountId:(NSString *) createAccountId
createCharacterId:(NSString *) createCharacterId;

- (void) logItem:(NSDate *)theDate
    theAccountId:(NSString *)theAccountId
  theCharacterId:(NSString *)theCharacterId
 theCharacterLvl:(long)theCharacterLvl
      theShardId:(long)theShardId
       theItemId:(long)theItemId
     theItemName:(NSString *)theItemName
    theItemCount:(int)theItemCount
       theAreaId:(long)theAreaId
     theAreaName:(NSString *)theAreaName
            theX:(float)theX
            theY:(float)theY
            theZ:(float)theZ;

-(void) logItemTransaction: (NSDate *) theDate
              theAccountId:(NSString *) theAccountId
            theCharacterId: (NSString *) theCharacterId
                theShardId: (long) theShardId
                 theItemId:(long) theItemId
               theItemName:(NSString *) theItemName
              theItemPrice:(float) theItemPrice
        theTransactionType:(int) theTransactionType
           theCurrencyType:(int) theCurrencyType
     theVirtualCurrencyLbl:(NSString *) theVirtualCurrencyLbl
          theCurrencyValue:(float) theCurrencyValue
    theVitualCurrencyCount:(int) theVirtualCurrencyCount;


- (void) loadCharacterCreate: (NSDate *) theDate
                theAccountId: (NSString *) theAccountId
              theCharacterId: (NSString *) theCharacterId
                theCharClass: (NSString *) theCharClass
             theCharSubClass:(NSString *) theCharSubClass
               theCharGender:(NSString *) theCharGender
                 theCharRace:(NSString *) theCharRace
                 theCharName:(NSString *) theCharName
                  theShardId: (long) theShardId;

- (void) loadCharacterDelete: (NSDate *) theDate
                theAccountId: (NSString *) theAccountId
              theCharacterId: (NSString *) theCharacterId;

- (void) loadShardCreate: (NSDate *) theDate
              theShardId: (long) theShardId
            theShardDesc: (NSString *) theShardDesc;

- (void) loadVirtualCurrency: (NSDate *) theDate
            theCurrencyLabel:(NSString *) theCurrencyLabel;

- (void) logCurrencyTransaction: (NSDate *) theDate
                   theAccountId: (NSString *) theAccountId
                 theCharacterId: (NSString *) theCharacterId
                     theShardId: (long) theShardId
             theTransactionType: (int) theTransactionType
                theCurrencyType: (int) theCurrencyType
          theVirtualCurrencyLbl: (NSString *) theVirtualCurrencyLbl
               theCurrencyValue: (float) theCurrencyValue
             theTransactionDesc: (NSString *) theTransactionDesc
        theVirtualCurrencyCount:(int)theVirtualCurrencyCount;

- (void) loadUgcRating: (NSDate *) theDate
          theAccountId: (NSString *) theAccountId
        theCharacterId: (NSString *) theCharacterId
            theShardId: (long) theShardId
             theItemId: (long) theItemId
           theItemName: (NSString *) theItemName
          theUgcRating:(NSString *)theUgcRating;

- (void) logEnterGroup: (NSDate *) theDate
          theAccountId: (NSString *) theAccountId
        theCharacterId: (NSString *) theCharacterId
       theCharacterLvl: (long) theCharacterLvl
            theShardId: (long) theShardId
            theGroupId: (long) theGroupId;

- (void) logExitGroup: (NSDate *) theDate
         theAccountId: (NSString *) theAccountId
       theCharacterId: (NSString *) theCharacterId
      theCharacterLvl: (long) theCharacterLvl
           theShardId: (long) theShardId
           theGroupId: (long) theGroupId;

- (void) logPlayerDeath: (NSDate *) theDate
           theAccountId: (NSString *) theAccountId
         theCharacterId: (NSString *) theCharacterId
             theShardId: (long) theShardId
         theKillerNpcId: (long) theKillerNpcId
           theDeathType: (NSString *) theDeathType
              theAreaId: (long) theAreaId
                   theX: (float) theX
                   theY: (float) theY
                   theZ: (float) theZ
            theAreaName:(NSString *) theAreaName;

- (void) logMessage: (NSDate *) theDate
          sendAccId: (NSString *) sendAccId
         sendCharId: (NSString *) sendCharId
          recvAccId: (NSString *) recvAccId
         recvCharId: (NSString *) recvCharId
         theShardId: (long) theShardId
      theChannelLbl: (NSString *) theChannelLbl
     theMessageDesc: (NSString *) theMessageDesc
theMessageCharCount: (long) theMessageCharCount;

- (void) logOgiRecv: (NSDate *) theDate
          sendAccId: (NSString *) sendAccId
         sendCharId: (NSString *) sendCharId
          recvAccId: (NSString *) recvAccId
         recvCharId: (NSString *) recvCharId
         theShardId: (long) theShardId
     theOgiLocation: (NSString *) theOgiLocation
     theOgiCategory: (NSString *) theOgiCategory
      theRecvAction: (NSString *) theRecvAction;

- (void) logOgiSend: (NSDate *) theDate
          sendAccId: (NSString *) sendAccId
         sendCharId: (NSString *) sendCharId
          recvAccId: (NSString *) recvAccId
         recvCharId: (NSString *) recvCharId
         theShardId: (long) theShardId
     theOgiLocation: (NSString *) theOgiLocation
     theOgiCategory: (NSString *) theOgiCategory;

- (void) logSocialEvent: (NSDate *) theDate
              sendAccId: (NSString *) sendAccId
             sendCharId: (NSString *) sendCharId
             theShardId: (long) theShardId
            sendCharLvl: (int) sendCharLvl
              recvAccId: (NSString *) recvAccId
             recvCharId: (NSString *) recvCharId
            recvCharLvl: (int) recvCharLvl
           theEventName: (NSString *) theEventName
           theEventType: (NSString *) theEventType;

- (void) putSubChange: (NSDate *) theDate
         theAccountId: (NSString *) theAccountId
         theSubStatus: (NSString *) theSubStatus
           theSubType: (NSString *) theSubType
          theSubValue: (float) theSubValue
          theExpireTs: (NSDate *) theExpireTs;

- (void) combatStart: (NSDate *) theDate
        theAccountId: (NSString *) theAccountId
      theCharacterId: (NSString *) theCharacterId
          theShardId: (long) theShardId
            theNpcId: (long) theNpcId
           theAreaId: (long) theAreaId
                theX: (float) theX
                theY: (float) theY
                theZ: (float) theZ
    theBeginAreaName:(NSString *) theBeginAreaName;

- (void) combatEnd: (NSDate *) theDate
      theAccountId: (NSString *) theAccountId
    theCharacterId: (NSString *) theCharacterId
        theShardId: (long) theShardId
          theNpcId: (long) theNpcId
         theAreaId: (long) theAreaId
              theX: (float) theX
              theY: (float) theY
              theZ: (float) theZ
    theEndAreaName:(NSString *) theEndAreaName;

- (void) logKillNpc: (NSDate *) theDate
       theAccountId: (NSString *) theAccountId
     theCharacterId: (NSString *) theCharacterId
       theShardId: (long) theShardId
    theCharacterLvl: (long) theCharacterLvl
           theNpcId: (long) theNpcId;

- (void) playerPoints: (NSDate * ) theDate
     theAccountId: (NSString *) theAccountId
   theCharacterId: (NSString *) theCharacterId
theCharacterLevel: (int) theCharacterLevel
       theShardId: (long) theShardId
     thePointType:(NSString *) thePointType
      theXpAmount: (long) theXpAmount
        isGrouped: (BOOL) isGrouped;

-(void) logPvpDuel: (NSDate *) theDate
      theAccountId: (NSString *) theAccountId
    theCharacterId: (NSString *) theCharacterId
        theShardId: (long) theShardId
          isWinner: (BOOL) isWinner;

- (void) levelBegin: (NSDate * ) theDate
       theAccountId: (NSString *) theAccountId
     theCharacterId: (NSString *) theCharacterId
  theCharacterLevel: (int) theCharacterLevel
         theShardId: (long) theShardId
         theLevelId: (long) theLevelId
               theX: (float) theX
               theY: (float) theY
               theZ: (float) theZ
        theAreaName:(NSString *) theAreaName;

- (void) levelEnd: (NSDate * ) theDate
     theAccountId: (NSString *) theAccountId
   theCharacterId: (NSString *) theCharacterId
theCharacterLevel: (int) theCharacterLevel
       theShardId: (long) theShardId
       theLevelId: (long) theLevelId
             theX: (float) theX
             theY: (float) theY
             theZ: (float) theZ
      theAreaName:(NSString *) theAreaName;


-(void) areaEnter: (NSDate *) theDate
     theAccountId: (NSString *) theAccountId
   theCharacterId: (NSString *) theCharacterId
  theCharacterLvl: (int) theCharacterLvl
       theShardId: (long) theShardId
        theAreaId: (long) theAreaId
             theX: (float) theX
             theY: (float) theY
             theZ: (float) theZ
      theAreaName:(NSString *) theAreaName;

-(void) areaExit: (NSDate *) theDate
    theAccountId: (NSString *) theAccountId
  theCharacterId: (NSString *) theCharacterId
 theCharacterLvl: (int) theCharacterLvl
      theShardId: (long) theShardId
       theAreaId: (long) theAreaId
            theX: (float) theX
            theY: (float) theY
            theZ: (float) theZ
     theAreaName:(NSString *) theAreaName;

-(void) logNpcInteraction: (NSDate *) theDate
             theAccountId: (NSString *) theAccountId
           theCharacterId: (NSString *) theCharacterId
               theShardId: (long) theShardId
             npcEventName: (NSString *) npcEventName
             npcEventType: (NSString *) npcEventType
                    npcId: (long) npcId
                  npcName: (NSString *) npcName
                  npcType: (NSString *) npcType
              theMinLevel: (int) theMinLevel
              theMaxLevel: (int) theMaxLevel
             theToughness: (int) theToughness
         theToughnessEnum: (NSString *) theToughnessEnum;

- (void) logChallenge: (NSDate *) theDate
         theAccountId: (NSString *) theAccountId
       theCharacterId: (NSString *) theCharacterId
           theShardId: (long) theShardId
          challengeId: (NSString *) challengeId
        challengeType: (NSString *) challengeType
      challengeStatus: (NSString *) challengeStatus;

-(void) recruitmentSend: (NSDate *) theDate
              sendAccId: (NSString *) sendAccId
             sendCharId: (NSString *) sendCharId
              recvAccId: (NSString *) recvAccId
             recvCharId: (NSString *) recvCharId
     theRecruitmentType: (NSString *) theRecruitmentType;

-(void) recruitmentReceive: (NSDate *) theDate
                 sendAccId: (NSString *) sendAccId
                sendCharId: (NSString *) sendCharId
                 recvAccId: (NSString *) recvAccId
                recvCharId: (NSString *) recvCharId
        theRecruitmentType: (NSString *) theRecruitmentType
     theRecruitmentOutcome: (NSString *) theRecruitmentOutcome;

- (void) storeCreate: (NSDate *) theDate
          theStoreId: (long) theStoreId
        theStoreDesc: (NSString *) theStoreDesc
       theStreetAddr: (NSString *) theStreetAddr
             theCity: (NSString *) theCity
            theState: (NSString *) theState
          theCountry: (NSString *) theCountry
       thePostalCode: (NSString *) thePostalCode;

- (void) storeDelete: (NSDate *) theDate
          theStoreId: (long) theStoreId;

- (void) storeBuyItem: (NSDate *) theDate
            sendAccId: (NSString *) sendAccId
           sendCharId: (NSString *) sendCharId
            recvAccId: (NSString *) recvAccId
           recvCharId: (NSString *) recvCharId
           theShardId: (long) theShardId
           theStoreId: (long) theStoreId
            theItemId: (long) theItemId
          theItemName: (NSString *) theItemName
         theItemPrice: (float) theItemPrice
      theCurrencyType: (int) theCurrencyType
theVirtualCurrencyLbl: (NSString *) theVirtualCurrencyLbl
     theCurrencyValue: (float) theCurrencyValue
theVirtualCurencyCount: (int) theVirtualCurrencyCount;

- (void) storeLogin: (NSDate *) theDate
       theAccountId: (NSString *) theAccountId
     theCharacterId: (NSString *) theCharacterId
         theShardId: (long) theShardId
         theStoreId: (long) theStoreId;

- (void) storeLogout: (NSDate *) theDate
        theAccountId: (NSString *) theAccountId
      theCharacterId: (NSString *) theCharacterId
          theShardId: (long) theShardId
          theStoreId: (long) theStoreId;

- (void) storeAddToCart: (NSDate *) theDate
           theAccountId: (NSString *) theAccountId
         theCharacterId: (NSString *) theCharacterId
             theStoreId: (long) theStoreId
              theItemId: (long) theItemId
            theItemName:(NSString *) theItemName
            theItemType:(NSString *) theItemType;

- (void) logEnterGuild: (NSDate *) theDate
          theAccountId: (NSString *) theAccountId
        theCharacterId: (NSString *) theCharacterId
       theCharacterLvl: (int) theCharacterLvl
            theShardId: (long) theShardId
            theGuildId: (long) theGuildId;

- (void) logExitGuild: (NSDate *) theDate
         theAccountId: (NSString *) theAccountId
       theCharacterId: (NSString *) theCharacterId
      theCharacterLvl: (int) theCharacterLvl
           theShardId: (long) theShardId
           theGuildId: (long) theGuildId
        theExitReason: (NSString *) theExitReason;

- (void) logFriendAdd: (NSDate *) theDate
            sendAccId: (NSString *) sendAccId
           sendCharId: (NSString *) sendCharId
            recvAccId: (NSString *) recvAccId
           recvCharId: (NSString *) recvCharId;

- (void) logFriendDelete: (NSDate *) theDate
               sendAccId: (NSString *) sendAccId
              sendCharId: (NSString *) sendCharId
               recvAccId: (NSString *) recvAccId
              recvCharId: (NSString *) recvCharId;

- (void) putGuildLeader: (NSDate *) theDate
           theAccountId: (NSString *) theAccountId
         theCharacterId: (NSString *) theCharacterId
             theGuildId: (long) theGuildId;

-(void) trackUgcCopy: (NSDate *) theDate
        theAccountId: (NSString *) theAccountId
      theCharacterId: (NSString *) theCharacterId
          theShardId: (long) theShardId
           theItemId: (long) theItemId
         theItemName:(NSString *) theItemName;

- (void) logFarmer: (NSDate *) theDate
      theAccountId: (NSString *) theAccountId
    theCharacterId: (NSString *) theCharacterId
        theShardId: (long) theShardId
     theFarmerType: (NSString *) theFarmerType;

- (void) logIntegrity: (NSDate *) theDate
         theAccountId:(NSString *) theAccountId
           theShardId:(long) theShardId
     theIntegrityType: (NSString *) theIntegrityType;

- (void) logCustomAction: (NSDate *) theDate
       theAccountId: (NSString *) theAccId
     theCharacterId: (NSString *) theCharacterId
    theCustomAction: (NSString *) theCustomAction
theCustomActionType: (NSString *) theCustomActionType
theCustomActionValue: (int) theCustomActionValue;

- (void) logReward: (NSDate *) theDate
   theAccountId: (NSString *) theAccountId
 theCharacterId: (NSString *) theCharacterId
     theShardId: (long) theShardId
     theRewardType: (long) theRewardType
       theRewardId: (NSString *) theRewardId
       theItemType: (long) theItemType
    theRewardCount: (float) theRewardCount;

- (void) logAreaDim: (NSDate *) theDate
       theAreaName:    (NSString *) theAreaName
         theAreaId:    (long)  theAreaId
          theMapId:    (long) theMapId
       theMapLabel:    (NSString *) theMapLabel
           theMinX:    (float) theMinX
           theMinY:    (float) theMinY
           theMinZ:    (float) theMinZ
           theMaxX:    (float) theMaxX
           theMaxY:    (float) theMaxY
           theMaxZ:    (float) theMaxZ;

- (void) logMob: (NSDate *) theDate
  theAccountId:    (NSString *) theAccountId
theCharacterId:    (NSString *) theCharacterId
    theShardId:    (long)  theShardId
theKillMobDesc: (NSString *) theKillMobDesc;


- (void) logCRMAction: (NSDate *)    theDate
        theAccountId: (NSString *) theAccountId
      theCharacterId: (NSString *) theCharacterId
          theShardId: (long)    theShardId
        theCrmAction: (NSString *) theCrmAction
    theCrmActionType: (NSString *) theCrmActionType
theCrmFulfilledStatus: (NSString *) theCrmFulfilledStatus
   theCrmFulfilledTs: (NSDate *) theCrmFulfilledTs;

- (void) logCustomerServiceAction: (NSDate *) theDate
             theAccountId: (NSString *) theAccountId
           theCharacterId: (NSString *) theCharacterId
               theShardId: (long)   theShardId
              theCsAction: (NSString *) theCsAction
          theCsActionType: (NSString *) theCsActionType;

- (void)    logProductivity: (NSDate *) theDate
            theAccountId: (NSString *) theAccountId
          thecharacterId: (NSString *) theCharacterId
              theShardId: (long)    theShardId
     theProductivityType: (NSString *) theProductivityType;

- (void) logResource: (NSDate *) theDate
     theAccountId: (NSString *) theAccountId
   thecharacterId: (NSString *) theCharacterId
       theShardId: (long)    theShardId
  theResourceType: (NSString *) theResourceType
    theResourceId: (NSString *) theResourceId
 theResourceCount: (long)   theResourceCount;

- (void) logAccountAd: (NSDate *) theDate
    theAccountId: (NSString *) theAccountId
    theAdTagName: (NSString *) theAdTagName
   theAdActionTs: (NSDate *) theAdActionTs;

- (void) logEconomic: (NSDate *) theDate
theSenderAccountId: (NSString *) theSenderAccountId
theSenderCharacterId: (NSString *) theSenderCharacterId
theReceiverAccountId: (NSString *) theReceiverAccountId
theReceiverCharacterId: (NSString *) theReceiverCharacterId
       theShardId: (long)  theShardId
        theItemId: (long) theItemId
      theItemName: (NSString *) theItemName
  theEconomicType: (NSString *) theEconomicType
 theEconomicValue: (float) theEconomicValue
  theCurrencyType: (NSString *) theCurrencyType
theVirtualCurrencyType: (NSString *) theVirtualCurrencyType
 theCurrencyValue: (float) theCurrencyValue
theVirtualCurrencyCount: (int ) theVirtualCurrencyCount;

- (void) logTrafficSource: (NSDate *) theDate
          theAccountId: (NSString *) theAccountId
      theTrafficSource: (NSString *) theTrafficSource
            theShardId: (long) theShardId
  theTrafficSourceType: (NSString *) theTrafficSoureType;







@end
