//
//  kAPI.m
//  Katana
//
//  Created by Jon Hovland on 3/17/13.
//
//

#import "kAPI.h"
#import "KatanaReachability.h"

@implementation kAPI

/**
 * Checks that the network is available
 * @return a boolean value that indicates yes or no
 */
- (BOOL)checkForNetwork
{
    // check if we've got network connectivity
    KatanaReachability *myNetwork = [KatanaReachability reachabilityWithHostName:@"google.com"];
    NetworkStatus myStatus = [myNetwork currentReachabilityStatus];
    
    switch (myStatus) {
        case NotReachable:
            //NSLog(@"There's no internet connection at all. Display error message now.");
            return NO;
            break;
            
        case ReachableViaWWAN:
           // NSLog(@"We have a 3G connection");
            return YES;
            break;
            
        case ReachableViaWiFi:
           // NSLog(@"We have WiFi.");
            return YES;
            break;
            
        default:
            break;
            
    }
    
    return NO;
    
}

/**
 * Standard initialization of the object, used to set up some class level variables that will be used often.
 * @param clientID NSString a string representation of the client's ID (provided by Ninjametrics)
 * @param applicationID NSString a string representation of the application's ID (provided by Ninjametrics)
 * @param password NSString plaintext representation of password for client/application pair (provided by Ninjametrics)
 * @return the initialized object
 */
-(id) initClientID: (int)clientID applicationID:(int) applicationID password:(NSString *) password{
    if (self = [super init]){
        _clientID = clientID;
        _appID = applicationID;
        _password = password;
        _filename = [self getFilePath];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager removeItemAtPath:_filename error:NULL];
    }
    
    
    return self;
}


/**
 * Converts an NSDate object into the format needed for the JSON object expected by the Katana API.
 * @param theDate NSDate the date to be converted to a formated string
 * @return the formatted date string
 */
- (NSString *) dateToJSON:(NSDate *) theDate{
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    [fmt setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS"];
    [fmt setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    
    NSString *jsonString = [fmt stringFromDate:theDate];
    
    NSString *returnString = [jsonString stringByAppendingString:@"Z"];
    
    return returnString;
    
}

/**
 * Constructs the URL used to send data to the Katana API
 * @param theAuthEnabled BOOL - A YES or NO to indicate whether passing the authentication params is required.
 * @return a string of the constructed URL
 */
-(NSString *) getEventURL:(BOOL) theAuthEnabled{
    
    NSString *aRetEventUrl = [API_BASE stringByAppendingString:EVENT_BASE];

    
    aRetEventUrl = [aRetEventUrl stringByAppendingString:@"?api_client="];
    aRetEventUrl = [aRetEventUrl stringByAppendingString:API_LIBRARY_USER_AGENT];
    
    if (theAuthEnabled) {
        aRetEventUrl = [aRetEventUrl stringByAppendingString:@"&client_id="];
        aRetEventUrl = [aRetEventUrl stringByAppendingString:[NSString stringWithFormat:@"%d",_clientID]];
        aRetEventUrl = [aRetEventUrl stringByAppendingString:@"&app_id="];
        aRetEventUrl = [aRetEventUrl stringByAppendingString:[NSString stringWithFormat:@"%d",_appID]];
        aRetEventUrl = [aRetEventUrl stringByAppendingString:@"&app_pwd="];
        aRetEventUrl = [aRetEventUrl stringByAppendingString:_password];
        
    }
    
    return aRetEventUrl;
}

/**
 * This creates the request object used to send to the Katana API
 * @param method NSString whether the request is a "GET" or "POST"
 * @param url NSString the target URL
 * @return a mutable request that can be used for Asynchronous type interactions.
 */
- (NSMutableURLRequest *) createRequestWithMethod:(NSString *) method url:(NSString *)url{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //for testing echo
    [request setValue:@"true" forHTTPHeaderField:@"echo-test"];
    
    
    return request;
}

/**
 * A call back method that is used during the asynchronous response if there's an error
 * @param connection NSURlConnection the connection used by the asynchronous operation
 * @param error NSError a pointer to an error that was returned from the operation
 */
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    NSLog(@"Error is : %@" , error);
}

/**
 * A call back method that is used during the asynchronous response if there's NOT an error
 * @param connection NSURlConnection the connection used by the asynchronous operation
 * @param response NSURLResponse pointer to response that was returned from the operation
 */
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int code = [httpResponse statusCode];
    
    NSLog(@"The http code returned was %d",code);
}


/**
 * This method returns the relative filepath for the file used to save data in this method
 * @return the filePath as an NSString
 */
-(NSString *) getFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:FILE_NAME];
    return filePath;
    
}

-(void) clearLocalFile
{

}

/**
 * This method takes the data and finally sends it to the Ninjametrics server
 * @param data NSMutableDictionary an event and its data
 */
-(void) sendDataToServer:(NSMutableDictionary *) data{
    
   /*[data setObject:_password forKey:kAPP_PWD];
   [data setObject:[NSNumber numberWithInt:_appID] forKey:kAPP_ID];
   [data setObject:[NSNumber numberWithInt:_clientID] forKey:kCLIENT_ID];
    */
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
    
   NSString * jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"json string=%@", jsonString);
    
    NSMutableURLRequest * aRequest = [self createRequestWithMethod:@"POST" url:[self getEventURL:YES]];
    
    if (aRequest){
        
        [aRequest setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [aRequest setHTTPBody:jsonData];
        
        NSLog(@"The host is %@",[[aRequest URL] host]);
        NSLog(@"The path is %@", [[aRequest URL] path]);
        NSLog(@"The query is %@",[[aRequest URL] query]);
        
        NSLog(@"Request body %@", [[NSString alloc] initWithData:[aRequest HTTPBody] encoding:NSUTF8StringEncoding]);
        
        [NSURLConnection connectionWithRequest:aRequest delegate:self];
    }
    
    
}

/**
 * This method takes the event array and finally sends it to the Ninjametrics server
 * as a jSON array (serialized to a json string)
 * @param data NSMutableArray : array of events+data
 */
-(void) sendArrayDataToServer:(NSMutableArray *) myArray{

    NSError *error;
    
    //serialize the entire array to json string
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:myArray options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString * jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"Serialized json string=%@", jsonString);
    
    NSMutableURLRequest * aRequest = [self createRequestWithMethod:@"POST" url:[self getEventURL:YES]];
    
    if (aRequest){
        
        [aRequest setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [aRequest setHTTPBody:jsonData];
        
        NSLog(@"The host is %@",[[aRequest URL] host]);
        NSLog(@"The path is %@", [[aRequest URL] path]);
        NSLog(@"The query is %@",[[aRequest URL] query]);
        
        NSLog(@"Request body %@", [[NSString alloc] initWithData:[aRequest HTTPBody] encoding:NSUTF8StringEncoding]);
        
        [NSURLConnection connectionWithRequest:aRequest delegate:self];
    }
}


/**
 * This method iterates through the saved events and sends the to the server
 */
-(void) dispositionData{
    
    NSLog(@"in disposition data");
    
    NSMutableArray *myArray = [NSMutableArray arrayWithContentsOfFile:_filename];
    
    NSLog(@"The array has %d items", [myArray count]);
    
    while ([myArray count] >= (CACHE_OBJECT_LIMIT>0?CACHE_OBJECT_LIMIT:1)) {
        NSMutableDictionary *firstObject = [myArray objectAtIndex:0];
        [myArray removeObjectAtIndex:0];
        
        [self sendDataToServer:firstObject];
    }
    
    //Write back the remaining event objects back into the file
    [myArray writeToFile:[self getFilePath] atomically:YES];
}

/**
 * This method iterates through the saved events,creates an event array 
 * and sends the array to the batch handler.
 */
-(void) dispositionDataArray{
    
    NSLog(@"in disposition data array");
    
    NSMutableArray *myArray = [NSMutableArray arrayWithContentsOfFile:_filename];
    
    if([myArray count] < CACHE_OBJECT_LIMIT) {
        return;
    }
    
    NSArray *tempObjectArray = [myArray subarrayWithRange:NSMakeRange(0, CACHE_OBJECT_LIMIT)];
    [myArray removeObjectsInRange:NSMakeRange(0, CACHE_OBJECT_LIMIT)];
    
    NSMutableArray *dispositionArray = [NSMutableArray arrayWithArray:tempObjectArray];
    
    if([dispositionArray count] >= 1)
        [self sendArrayDataToServer:dispositionArray];
    
    //overwrite the file
    [myArray writeToFile:[self getFilePath] atomically:YES];
}


/**
 * Add the event to the event queue.
 * Once the number of events exceeds CACHE_OBJECT_LIMIT,
 * the data will be set for immediate dispositioning.
 *
 * @param theEvent NSMutableDictionary data for the event object
 */
- (void) logEvent:(NSMutableDictionary *) theEvent{
    
    NSMutableArray *myArray = [NSMutableArray arrayWithContentsOfFile:_filename];
    
    if (myArray == nil) {
        NSLog(@"There was no file to create an array from, initializing array");
        myArray = [[NSMutableArray alloc]init];
    }
    
    [myArray addObject:theEvent];
    [myArray writeToFile:_filename atomically:YES];
    
    
    if ([self checkForNetwork]){
        [self dispositionDataArray];
    }

}


#pragma mark - Account methods
/**
 * load Account creation data. this must be run once per Account Id, before
 * referencing said Account Id in any other logging statements.
 *
 * @param theDate Date - when was the Account created?
 * @param theAccountId String - the Account UUID to initialize
 * @param theAccountType String - acceptable values: Free, Paid, Premium
 * @param theAccLang String - case sensitive IETF language tag - RFC 4646.
 *            from geonames.org/countries
 * @param theAccCountry String - ISO-3166 alpha2 from geonames.org/countries
 * @param theAccGender String - acceptable values: "M", "F", "T", ""
 * @param theAccDateOfBirth Date - the reported DOB of the Account user
 * @param theAccCurrencyBal float - OPTIONAL: insert 0 if unused
 * @param thePlatform String - what platform was the Account created on?
 *            user-agent string, "mobile" or, "desktop". open-ended, but
 *            should be used consistently.
 */
- (void) loadAccountCreate:(NSDate *) theDate
               theAccountId:(NSString *) theAccountId
             theAccountType:(NSString *) theAccountType
                 theAccLang:(NSString *) theAccLang
              theAccCountry:(NSString *) theAccCountry
               theAccGender:(NSString *) theAccGender
          theAccDateOfBirth:(NSDate *) theAccDateOfBirth
          theAccCurrencyBal:(double) theAccCurrencyBal
                thePlatform:(NSString *) thePlatform


{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cACCOUNT_CREATE] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theAccountType forKey:kACCOUNT_SUBSCRIPTION_TYPE];
    [theEventData setObject:theAccLang forKey:kACCOUNT_LANGUAGE];
    [theEventData setObject:theAccCountry forKey:kACCOUNT_COUNTRY];
    [theEventData setObject:theAccGender forKey:kACCOUNT_GENDER];
    [theEventData setObject:[self dateToJSON:theAccDateOfBirth] forKey:kACCOUNT_DATE_OF_BIRTH];
    [theEventData setObject:[NSNumber numberWithDouble:theAccCurrencyBal] forKey:kACCOUNT_CURRENCY_BALANCE];
    [theEventData setObject:thePlatform forKey:kPLATFORM];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    [self logEvent:theEvent];
    
}


/**
 * load Account deletion data. this must be run once per Account Id life
 * cycle, to properly keep application metrics in-sync.
 *
 * @param theDate Date - when did the Account deletion occur?
 * @param theAccountId String - the Account UUID that was deleted
 * @param theChurnType String - acceptable values: Stopped, Expired, Failed,
 *            Cancelled
 * @param isFarmer boolean - was this deleted Account a gold farmer?
 * @param isIntegrity boolean - was this deleted Account's integrity
 *            compromised?
 */
-(void) loadAccountDelete:(NSDate *) theDate
              theAccountId:(NSString *) theAccountId
              theChurnType:(NSString *) theChurnType{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cACCOUNT_DELETE] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theChurnType forKey:kACCOUNT_STATUS];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * log that a Login event occurred
 *
 * @param theDate Date - what time did the login occur at?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param thePlatform String - what platform did the Event happen on?
 *            user-agent string, "mobile" or, "desktop". open-ended, but
 *            should be used consistently.
 * @param theAreaId long - an in-app/game location id the user/player
 *            started in. OPTIONAL: insert 0 if unused.
 */
- (void) logLogin:(NSDate *) theDate
     theAccountId:(NSString *) theAccountId
   theCharacterId:(NSString *) theCharacterId
       theShardId:(long) theShardId
      thePlatform:(NSString *) thePlatform
        theAreaId:(long) theAreaId
 theAreaName:(NSString *)theAreaName
{
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    //assign the class level variables
    
    
    _theAccountId = theAccountId;
    _theCharacterId = theCharacterId;
    _theShardID = theShardId;
    _thePlatform = thePlatform;
    _theAreaId = theAreaId;
    _theAreaName = theAreaName;
    
    [theEventData setObject:[NSNumber numberWithInt:cLOGIN] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:thePlatform forKey:kPLATFORM];
    [theEventData setObject:[NSNumber numberWithLong:theAreaId] forKey:kAREA_ID];
    [theEventData setObject:theAreaName forKey:kAREA_NAME];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
    //also schedule the heartbeat here
    _myTimer = [[NSTimer alloc]init];
    _myTimer = [NSTimer scheduledTimerWithTimeInterval:HEARTBEAT_INTERVAL target:self selector:@selector(heartBeat) userInfo:nil repeats:YES];
    
}


/**
 This is the periodic ping to the system with a "logout" event type
 */
-(void) heartBeat{
    
    NSDate *heartbeatNow = [NSDate date];
    
    // don't want to invalidate the timer, so copy the logout code
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:heartbeatNow] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cLOGOUT] forKey:kTYPE];
    [theEventData setObject:_theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:_theCharacterId forKey:kCHARACTER_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}



/**
 * log that a Logout event occurred
 *
 * @param theDate Date - what time did the logout occur at?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param thePlatform String - what platform did the Event happen on?
 *            user-agent string, "mobile" or, "desktop". open-ended, but
 *            should be used consistently.
 * @param theAreaId long - an in-app/game location id the user/player exited
 *            from. OPTIONAL: insert 0 if unused.
 */
- (void) logLogout:(NSDate *) theDate
      theAccountId:(NSString *) theAccountId
    theCharacterId:(NSString *) theCharacterId
{
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cLOGOUT] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
    
    [_myTimer invalidate];
    _myTimer = nil;
    
    
}

/**
 * log a message between two users
 *
 * @param theDate Date - when did the message occur?
 * @param sendAccId String - the sender's account id
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvAccId String - the reciever's account id
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theChannelLbl String - examples: "chat", "im", "teamspeak"
 * @param theMessageDesc String - OPTIONAL: "" when not used
 * @param theMessageCharCount long
 */
- (void) logMessage: (NSDate *) theDate
          sendAccId: (NSString *) sendAccId
         sendCharId: (NSString *) sendCharId
          recvAccId: (NSString *) recvAccId
         recvCharId: (NSString *) recvCharId
         theShardId: (long) theShardId
      theChannelLbl: (NSString *) theChannelLbl
     theMessageDesc: (NSString *) theMessageDesc
theMessageCharCount: (long) theMessageCharCount{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cMESSAGE] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    
    [theEventData setObject:theChannelLbl forKey:kMESSAGE_CHANNEL_LABEL];
    [theEventData setObject:theMessageDesc forKey:kMESSAGE_DESCRIPTION];
    [theEventData setObject:[NSNumber numberWithLong:theMessageCharCount] forKey:kMESSAGE_CHARACTER_COUNT];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
    
}

/**
 * log a friend add
 *
 * @param theDate Date - when did the sender add the receiver?
 * @param sendAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 */

- (void) logFriendAdd: (NSDate *) theDate
            sendAccId: (NSString *) sendAccId
           sendCharId: (NSString *) sendCharId
            recvAccId: (NSString *) recvAccId
           recvCharId: (NSString *) recvCharId{
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cFRIEND_ADD] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}

/**
 * log a friend delete
 *
 * @param theDate Date - when did the sender delete the receiver?
 * @param sendAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 */
- (void) logFriendDelete: (NSDate *) theDate
               sendAccId: (NSString *) sendAccId
              sendCharId: (NSString *) sendCharId
               recvAccId: (NSString *) recvAccId
              recvCharId: (NSString *) recvCharId{
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cFRIEND_DELETE] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}

/**
 * log the end (receiving) of a social transaction outside of the
 * application aka an Off-Game Interaction
 *
 * @param theDate Date - what time did the event occur at?
 * @param sendAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theOgiLocation String - a unique identifier for the location,
 *            preferably a URI
 * @param theOgiCategory String - examples: "gift", "forum post", "click"
 * @param theRecvAction String - examples: "repost", "RT", "forum reply"
 */
- (void) logOgiRecv: (NSDate *) theDate
          sendAccId: (NSString *) sendAccId
         sendCharId: (NSString *) sendCharId
          recvAccId: (NSString *) recvAccId
         recvCharId: (NSString *) recvCharId
         theShardId: (long) theShardId
     theOgiLocation: (NSString *) theOgiLocation
     theOgiCategory: (NSString *) theOgiCategory
      theRecvAction: (NSString *) theRecvAction{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cOFF_GAME_INTERACTION_RECEIVE] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    
    [theEventData setObject:theOgiLocation forKey:kOGI_LOCATION];
    [theEventData setObject:theOgiCategory forKey:kOGI_CATEGORY];
    [theEventData setObject:theRecvAction forKey:kOGI_RECEIVER_ACTION];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}

/**
 * log the start (sending) of a social transaction outside of the
 * application aka an Off-Game Interaction
 *
 * @param theDate Date - what time did the event occur at?
 * @param sendAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theOgiLocation String - a unique identifier for the location,
 *            preferably a URI
 * @param theOgiCategory String - examples: "gift", "forum post", "click"
 */
- (void) logOgiSend: (NSDate *) theDate
          sendAccId: (NSString *) sendAccId
         sendCharId: (NSString *) sendCharId
          recvAccId: (NSString *) recvAccId
         recvCharId: (NSString *) recvCharId
         theShardId: (long) theShardId
     theOgiLocation: (NSString *) theOgiLocation
     theOgiCategory: (NSString *) theOgiCategory{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cOFF_GAME_INTERACTION_SEND] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    
    [theEventData setObject:theOgiLocation forKey:kOGI_LOCATION];
    [theEventData setObject:theOgiCategory forKey:kOGI_CATEGORY];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}

/**
 * log an in-app Social event. DO NOT USE for game invites, messages,
 * group enter/exit, store gifts, or clan enter/exit. this should be used
 * for social events not already explicity called out.
 *
 * @param theDate Date - what time did the event occur at?
 * @param sendAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param sendCharLvl int - the character's level/XP. OPTIONAL: insert 0 if
 *            unused.
 * @param recvAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvCharLvl int - the character's level/XP. OPTIONAL: insert 0 if
 *            unused.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theEventName String - the name of the in-game social event.
 *            consistently names these.
 * @param theEventType String - examples: "clan invite", "group invite", "barter"
 */
- (void) logSocialEvent: (NSDate *) theDate
              sendAccId: (NSString *) sendAccId
             sendCharId: (NSString *) sendCharId
             theShardId: (long) theShardId
            sendCharLvl: (int) sendCharLvl
              recvAccId: (NSString *) recvAccId
             recvCharId: (NSString *) recvCharId
            recvCharLvl: (int) recvCharLvl
           theEventName: (NSString *) theEventName
           theEventType: (NSString *) theEventType{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cSOCIAL] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:sendCharLvl] forKey:kSENDER_CHARACTER_LEVEL];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:recvCharLvl] forKey:kRECEIVER_CHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theEventName forKey:kSOCIAL_EVENT_NAME];
    [theEventData setObject:theEventType forKey:kSOCIAL_EVENT_TYPE];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
}

/**
 * update the NM service with new information about an account subscription.
 * theExpireTs - theDate = subscription period length
 *
 * @param theDate Date - when did the subscription start?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theSubStatus String - acceptable values: Active, Stopped, Expired,
 *            Failed, Cancelled
 * @param theSubType String - acceptable values: Free, Paid, Premium
 * @param theSubValue float - amount paid by the user for the subscription
 *            for the relevant/current pay period. example: 7.34 (for June)
 * @param theExpireTs Date - when does the subscription expire?
 */
- (void) putSubChange: (NSDate *) theDate
         theAccountId: (NSString *) theAccountId
         theSubStatus: (NSString *) theSubStatus
           theSubType: (NSString *) theSubType
          theSubValue: (float)  theSubValue
          theExpireTs: (NSDate *) theExpireTs
{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCHANGE_SUBSCRIPTION] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theSubStatus forKey:kACCOUNT_STATUS];
    [theEventData setObject:theSubType forKey:kACCOUNT_SUBSCRIPTION_TYPE];
    [theEventData setObject:[NSNumber numberWithFloat:theSubValue] forKey:kACCOUNT_SUBSCRIPTION_VALUE];
    [theEventData setObject:[self dateToJSON:theExpireTs] forKey:kACCOUNT_SUBSCRIPTION_EXPIRES];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}


#pragma mark - Character Methods

//[_api loadCharacterCreate:now theAccountId:@"1234" theCharacterId:@"more blahs" theCharClass:@"Xome class" theCharSubClass:@"the subclass" theCharGender:@"female" theCharRace:@"elf" theCharName:@"Ishmar" theShardId:0];

/**
 * load Character creation data. this must be run once per Character Id,
 * before referencing said Character Id in any other logging statements.
 *
 * @param theDate Date - when did the character creation happen?
 * @param theAccountId String - previously initialized Account UUID
 * @param theCharacterId String - the Account unique Character Id to
 *            initialize
 * @param theCharClass String - higher-level Character Class. open-ended,
 *            but be consistent.
 * @param theCharSubClass String - further specific classification.
 * @param theCharGender String - the Character's gender must be "M", "F" or "T"
 * @param theCharRace String - Character's in-game race
 * @param theCharName String - the name of the in-game Character
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 */
- (void) loadCharacterCreate:(NSDate *)theDate theAccountId:(NSString *)theAccountId theCharacterId:(NSString *)theCharacterId theCharClass:(NSString *)theCharClass theCharSubClass:(NSString *)theCharSubClass theCharGender:(NSString *)theCharGender theCharRace:(NSString *)theCharRace theCharName:(NSString *)theCharName theShardId:(long)theShardId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCHARACTER_CREATE] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:theCharClass forKey:kCHARACTER_CLASS];
    [theEventData setObject:theCharSubClass forKey:kCHARACTER_SUB_CLASS];
    [theEventData setObject:theCharGender forKey:kCHARACTER_GENDER];
    [theEventData setObject:theCharRace forKey:kCHARACTER_RACE];
    [theEventData setObject:theCharName forKey:kCHARACTER_NAME];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
}

/**
 * load Character deletion data. this must be at the end of a Character life
 * cycle, to properly keep application metrics in-sync.
 *
 * @param theDate Date - when was the Character deleted?
 * @param theAccountId String - previously initialized Account UUID
 * @param theCharacterId String - identifier of Character that was deleted
 *            from the Account
 */
- (void) loadCharacterDelete: (NSDate *) theDate
                theAccountId: (NSString *) theAccountId
              theCharacterId: (NSString *) theCharacterId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCHARACTER_DELETE] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}

/**
 * log a player's death at the hands of an NPC
 *
 * @param theDate Date - when did the player death occur?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theKillerNpcId long - the NPC that killed the player
 * @param theDeathType String - NOT USED for now ... use ""
 * @param theAreaId long
 * @param theX float
 * @param theY float
 * @param theZ float
 */
- (void) logPlayerDeath: (NSDate *) theDate
           theAccountId: (NSString *) theAccountId
         theCharacterId: (NSString *) theCharacterId
             theShardId: (long) theShardId
         theKillerNpcId: (long) theKillerNpcId
           theDeathType: (NSString *) theDeathType
              theAreaId: (long) theAreaId
                   theX: (float) theX
                   theY: (float) theY
                   theZ: (float) theZ
            theAreaName:(NSString *) theAreaName

{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cPLAYER_DEATH] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong: theKillerNpcId] forKey:kNPC_ID];
    [theEventData setObject:theDeathType forKey:kPLAYER_DEATH_TYPE];
    [theEventData setObject:[NSNumber numberWithLong:theAreaId] forKey:kAREA_ID];
    [theEventData setObject:[NSNumber numberWithFloat:theX] forKey:kPOSITION_X];
    [theEventData setObject:[NSNumber numberWithFloat:theY] forKey:kPOSITION_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theZ] forKey:kPOSITION_Z];
    [theEventData setObject:theAreaName forKey:kAREA_NAME];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * kick off the beginning of combat
 *
 * @param theDate Date - when did combat start?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theNpcId long - the id of the NPC being fought
 * @param theAreaId long
 * @param theX float
 * @param theY float
 * @param theZ float
 */
- (void) combatStart: (NSDate *) theDate
        theAccountId: (NSString *) theAccountId
      theCharacterId: (NSString *) theCharacterId
          theShardId: (long) theShardId
            theNpcId: (long) theNpcId
           theAreaId: (long) theAreaId
                theX: (float) theX
                theY: (float) theY
                theZ: (float) theZ
    theBeginAreaName:(NSString *) theBeginAreaName
{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCOMBAT_BEGIN] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong: theNpcId] forKey:kNPC_ID];
    [theEventData setObject:[NSNumber numberWithLong:theAreaId] forKey:kAREA_ID];
    [theEventData setObject:[NSNumber numberWithFloat:theX] forKey:kPOSITION_X];
    [theEventData setObject:[NSNumber numberWithFloat:theY] forKey:kPOSITION_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theZ] forKey:kPOSITION_Z];
    [theEventData setObject:theBeginAreaName forKey:kAREA_NAME];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
}

/**
 * end of combat
 *
 * @param theDate Date - when did combat end?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theNpcId long - the id of the NPC fought
 * @param theAreaId long
 * @param theX float
 * @param theY float
 * @param theZ float
 */
- (void) combatEnd: (NSDate *) theDate
      theAccountId: (NSString *) theAccountId
    theCharacterId: (NSString *) theCharacterId
        theShardId: (long) theShardId
          theNpcId: (long) theNpcId
         theAreaId: (long) theAreaId
              theX: (float) theX
              theY: (float) theY
              theZ: (float) theZ
    theEndAreaName:(NSString *) theEndAreaName
{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCOMBAT_END] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong: theNpcId]forKey:kNPC_ID];
    [theEventData setObject:[NSNumber numberWithLong:theAreaId] forKey:kAREA_ID];
    [theEventData setObject:[NSNumber numberWithFloat:theX] forKey:kPOSITION_X];
    [theEventData setObject:[NSNumber numberWithFloat:theY] forKey:kPOSITION_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theZ] forKey:kPOSITION_Z];
    [theEventData setObject:theEndAreaName forKey:kAREA_NAME];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
}

/**
 * log a player killing an NPC
 *
 * @param theDate Date - when did the kill happen?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLvl long - the numeric level identifier the character
 *            currently is at. OPTIONAL: insert 0 if unused.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theNpcId long - the id of the NPC fought
 */
- (void) logKillNpc: (NSDate *) theDate
       theAccountId: (NSString *) theAccountId
     theCharacterId: (NSString *) theCharacterId
         theShardId: (long) theShardId
    theCharacterLvl: (long)theCharacterLvl
           theNpcId: (long) theNpcId{
    
    
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cKILL] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theCharacterLvl] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong: theNpcId] forKey:kNPC_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}

/**
 * log a player points change
 *
 * @param theDate Date - when did the XP get bestowed?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLevel Integer
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theXpAmount long
 * @param isGrouped boolean - did the XP result from a group event?
 */
- (void) playerPoints: (NSDate * ) theDate
     theAccountId: (NSString *) theAccountId
   theCharacterId: (NSString *) theCharacterId
theCharacterLevel: (int) theCharacterLevel
       theShardId: (long) theShardId
         thePointType:(NSString *) thePointType
          theXpAmount: (long) theXpAmount
        isGrouped: (BOOL) isGrouped{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cPLAYER_XP] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:theCharacterLevel] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:thePointType forKey:kPOINT_TYPE];
    [theEventData setObject:[NSNumber numberWithLong:theXpAmount] forKey:kPLAYER_XP_AMOUNT];
    [theEventData setObject:isGrouped?@"Y":@"N" forKey:kFLAG_GROUPED];
    
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}

/**
 * log a player's interaction in a PVP Duel
 *
 * @param theDate Date - when did the duel occur?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param isWinner boolean - did this player win?
 */
-(void) logPvpDuel: (NSDate *) theDate
      theAccountId: (NSString *) theAccountId
    theCharacterId: (NSString *) theCharacterId
        theShardId: (long) theShardId
          isWinner: (BOOL) isWinner{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cPVP_DUEL] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:isWinner?@"Y":@"N" forKey:kFLAG_WIN];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}
#pragma mark - Guild methods
/**
 * log a Guild ENTER event
 *
 * @param theDate Date - when did the player enter the guild?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLvl long - the numeric level identifier the character
 *            currently is at. OPTIONAL: insert 0 if unused.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theGuildId long
 * @param isLeader boolean - is this player the leader?
 */
- (void) logEnterGuild: (NSDate *) theDate
          theAccountId: (NSString *) theAccountId
        theCharacterId: (NSString *) theCharacterId
       theCharacterLvl: (int) theCharacterLvl
            theShardId: (long) theShardId
            theGuildId: (long) theGuildId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cGUILD_ENTER] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:theCharacterLvl] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theGuildId] forKey:kGUILD_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}

/**
 * log a Guild EXIT event
 *
 * @param theDate Date - when did the player exit the guild?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLvl long - the numeric level identifier the character
 *            currently is at. OPTIONAL: insert 0 if unused.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theGuildId long
 * @param theExitReason String - examples: "hacking", "bad team"
 */
- (void) logExitGuild: (NSDate *) theDate
         theAccountId: (NSString *) theAccountId
       theCharacterId: (NSString *) theCharacterId
      theCharacterLvl: (int) theCharacterLvl
           theShardId: (long) theShardId
           theGuildId: (long) theGuildId
        theExitReason: (NSString *) theExitReason{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cGUILD_EXIT] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:theCharacterLvl] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theGuildId] forKey:kGUILD_ID];
    [theEventData setObject:theExitReason forKey:kGUILD_EXIT_REASON];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}
/**
 * log a change in Guild leaders
 *
 * @param theDate Date - when did the player exit the guild?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theGuildId long
 */
- (void) putGuildLeader: (NSDate *) theDate
           theAccountId: (NSString *) theAccountId
         theCharacterId: (NSString *) theCharacterId
             theGuildId: (long) theGuildId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cGUILD_LEADER] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theGuildId] forKey:kGUILD_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
}



#pragma mark - Leveling methods

/**
 * log the player beginning a level
 *
 * @param theDate Date - when did the player begin?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLevel Integer - the level of the character
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theLevelId long
 * @param theX float
 * @param theY float
 * @param theZ float
 */
- (void) levelBegin: (NSDate * ) theDate
       theAccountId: (NSString *) theAccountId
     theCharacterId: (NSString *) theCharacterId
  theCharacterLevel: (int) theCharacterLevel
         theShardId: (long) theShardId
         theLevelId: (long) theLevelId
               theX: (float) theX
               theY: (float) theY
               theZ: (float) theZ
        theAreaName:(NSString *) theAreaName{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cLEVEL_BEGIN] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:theCharacterLevel] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theLevelId] forKey:kAREA_ID]; //TODO:should have a second constant?
    [theEventData setObject:[NSNumber numberWithFloat:theX] forKey:kPOSITION_X];
    [theEventData setObject:[NSNumber numberWithFloat:theY] forKey:kPOSITION_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theZ] forKey:kPOSITION_Z];
    [theEventData setObject:theAreaName forKey:kAREA_NAME];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}

/**
 * log the player completing a level
 *
 * @param theDate Date - when did the player complete the level?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLevel Integer - the level of the character
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theLevelId long
 * @param theX float
 * @param theY float
 * @param theZ float
 */
- (void) levelEnd: (NSDate * ) theDate
     theAccountId: (NSString *) theAccountId
   theCharacterId: (NSString *) theCharacterId
theCharacterLevel: (int) theCharacterLevel
       theShardId: (long) theShardId
       theLevelId: (long) theLevelId
             theX: (float) theX
             theY: (float) theY
             theZ: (float) theZ
      theAreaName:(NSString *) theAreaName{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cLEVEL_END] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:theCharacterLevel] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theLevelId] forKey:kAREA_ID]; //TODO:should have a second constant?
    [theEventData setObject:[NSNumber numberWithFloat:theX] forKey:kPOSITION_X];
    [theEventData setObject:[NSNumber numberWithFloat:theY] forKey:kPOSITION_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theZ] forKey:kPOSITION_Z];
    [theEventData setObject:theAreaName forKey:kAREA_NAME];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}

/**
 * log the player entering an area
 *
 * @param theDate Date - when did the player enter the area?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLevel Integer - the level of the character
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theAreaId long
 * @param theX float
 * @param theY float
 * @param theZ float
 */
-(void) areaEnter: (NSDate *) theDate
     theAccountId: (NSString *) theAccountId
   theCharacterId: (NSString *) theCharacterId
  theCharacterLvl: (int) theCharacterLvl
       theShardId: (long) theShardId
        theAreaId: (long) theAreaId
             theX: (float) theX
             theY: (float) theY
             theZ: (float) theZ
theAreaName:(NSString *) theAreaName{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cAREA_ENTER] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:theCharacterLvl] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theAreaId] forKey:kAREA_ID];
    [theEventData setObject:[NSNumber numberWithFloat:theX] forKey:kPOSITION_X];
    [theEventData setObject:[NSNumber numberWithFloat:theY] forKey:kPOSITION_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theZ] forKey:kPOSITION_Z];
    [theEventData setObject:theAreaName forKey:kAREA_NAME];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}


-(void) areaExit: (NSDate *) theDate
    theAccountId: (NSString *) theAccountId
  theCharacterId: (NSString *) theCharacterId
 theCharacterLvl: (int) theCharacterLvl
      theShardId: (long) theShardId
       theAreaId: (long) theAreaId
            theX: (float) theX
            theY: (float) theY
            theZ: (float) theZ
     theAreaName:(NSString *) theAreaName{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cAREA_EXIT] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithInt:theCharacterLvl] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theAreaId] forKey:kAREA_ID];
    [theEventData setObject:[NSNumber numberWithFloat:theX] forKey:kPOSITION_X];
    [theEventData setObject:[NSNumber numberWithFloat:theY] forKey:kPOSITION_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theZ] forKey:kPOSITION_Z];
    [theEventData setObject:theAreaName forKey:kAREA_NAME];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * log the interaction between an in-game NPC and a player
 *
 * @param theDate Date - when did the interaction take place?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param npcEventName String
 * @param npcEventType String - examples: "Combat", "Purchase", "quest"
 * @param npcId long
 * @param npcName String - examples: "the Mort of T", "Duque de Zeta"
 * @param npcType String - examples: "Vendor", "Quest Giver", "Enemy"
 * @param theMinLevel Integer - the lowest level this NPC occurs on
 * @param theMaxLevel Integer - the highest level this NPC occurs on
 * @param theToughness Integer - a numeric representation of the NPC's
 *            ability, example: 0.25
 * @param theToughnessEnum String - textual representation of the NPC's
 *            toughness, example: quarter
 */
//TODO: point out that the descriptor for toughness is not allowable for an int
-(void) logNpcInteraction: (NSDate *) theDate
             theAccountId: (NSString *) theAccountId
           theCharacterId: (NSString *) theCharacterId
               theShardId: (long) theShardId
             npcEventName: (NSString *) npcEventName
             npcEventType: (NSString *) npcEventType
                    npcId: (long) npcId
                  npcName: (NSString *) npcName
                  npcType: (NSString *) npcType
              theMinLevel: (int) theMinLevel
              theMaxLevel: (int) theMaxLevel
             theToughness: (int) theToughness
         theToughnessEnum: (NSString *) theToughnessEnum{
    
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cNPC_INTERACTION] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:npcEventName forKey:kNPC_EVENT_NAME];
    [theEventData setObject:npcEventType forKey:kNPC_EVENT_TYPE];
    [theEventData setObject:[NSNumber numberWithLong: npcId]forKey:kNPC_ID];
    [theEventData setObject:npcName forKey:kNPC_NAME];
    [theEventData setObject:npcType forKey:kNPC_TYPE];
    [theEventData setObject:[NSNumber numberWithInt:theMinLevel] forKey:kNPC_LEVEL_MIN];
    [theEventData setObject:[NSNumber numberWithInt:theMaxLevel] forKey:kNPC_LEVEL_MAX];
    [theEventData setObject:[NSNumber numberWithInt:theToughness] forKey:kNPC_TOUGHNESS];
    [theEventData setObject:theToughnessEnum forKey:kNPC_TOUGHNESS_ENUM];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}

/**
 * log a player challenge starting/working/done
 *
 * @param theDate Date - timestamp of updates to challenge
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param challengeId String
 * @param challengeType String - examples: "item search", "kill NPC"
 * @param challengeStatus String - ONLY: Accepted, Completed, Abandonded
 */
- (void) logChallenge: (NSDate *) theDate
         theAccountId: (NSString *) theAccountId
       theCharacterId: (NSString *) theCharacterId
           theShardId: (long) theShardId
          challengeId: (NSString *) challengeId
        challengeType: (NSString *) challengeType
      challengeStatus: (NSString *) challengeStatus{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCHALLENGE] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:challengeId forKey:kCHALLENGE_ID];
    [theEventData setObject:challengeType forKey:kCHALLENGE_TYPE];
    [theEventData setObject:challengeStatus forKey:kCHALLENGE_STATUS];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
}

/**
 * log that a recruitment message was sent
 *
 * @param theDate Date - when was the recruitment message sent?
 * @param sendAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theRecruitmentType String - open ended String field. suggested
 *            options: "mobile ad", "banner", "promotion"
 */
-(void) recruitmentSend: (NSDate *) theDate
              sendAccId: (NSString *) sendAccId
             sendCharId: (NSString *) sendCharId
              recvAccId: (NSString *) recvAccId
             recvCharId: (NSString *) recvCharId
     theRecruitmentType: (NSString *) theRecruitmentType{
    
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cRECRUITMENT_SEND] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    [theEventData setObject:theRecruitmentType forKey:kRECRUITMENT_TYPE];
    
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}
/**
 * log that a recruitment message was responded to
 *
 * @param theDate Date - when was the response received?
 * @param sendAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theRecruitmentType String - open ended String field. suggested
 *            options: "mobile ad", "banner", "promotion"
 * @param theRecruitmentOutcome String - open ended String field. suggested
 *            options: "sign-up", "blocked", "click"
 */
-(void) recruitmentReceive: (NSDate *) theDate
                 sendAccId: (NSString *) sendAccId
                sendCharId: (NSString *) sendCharId
                 recvAccId: (NSString *) recvAccId
                recvCharId: (NSString *) recvCharId
        theRecruitmentType: (NSString *) theRecruitmentType
     theRecruitmentOutcome: (NSString *) theRecruitmentOutcome{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cRECRUITMENT_RECEIVE] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    [theEventData setObject:theRecruitmentType forKey:kRECRUITMENT_TYPE];
    [theEventData setObject:theRecruitmentOutcome forKey:kRECRUITMENT_OUTCOME];
    
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}

#pragma mark - Store methods
/**
 * add a record of a store to the backing DIM table
 *
 * @param theDate Date
 * @param theStoreId long
 * @param theStoreDesc String
 * @param theStreetAddr String
 * @param theCity String
 * @param theState String
 * @param theCountry String
 * @param thePostalCode String
 */
- (void) storeCreate: (NSDate *) theDate
          theStoreId: (long) theStoreId
        theStoreDesc: (NSString *) theStoreDesc
       theStreetAddr: (NSString *) theStreetAddr
             theCity: (NSString *) theCity
            theState: (NSString *) theState
          theCountry: (NSString *) theCountry
       thePostalCode: (NSString *) thePostalCode{
    
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cSTORE_CREATE] forKey:kTYPE];
    [theEventData setObject:[NSNumber numberWithLong:theStoreId] forKey:kSTORE_ID];
    [theEventData setObject:theStoreDesc forKey:kSTORE_DESCRIPTION];
    [theEventData setObject:theStreetAddr forKey:kMAP_REAL_ADDRESS];
    [theEventData setObject:theCity forKey:kMAP_REAL_CITY];
    [theEventData setObject:theState forKey:kMAP_REAL_STATE];
    [theEventData setObject:theCountry forKey:kMAP_REAL_COUNTRY];
    [theEventData setObject:thePostalCode forKey:kMAP_REAL_POSTAL_CODE];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
    
    
}
/**
 * remove a store's record from the backing DIM table
 *
 * @param theDate Date
 * @param theStoreId long
 */
- (void) storeDelete: (NSDate *) theDate
          theStoreId: (long) theStoreId{
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cSTORE_DELETE] forKey:kTYPE];
    [theEventData setObject:[NSNumber numberWithLong:theStoreId] forKey:kSTORE_ID];
    
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
}
/**
 * log a player buying a store item
 *
 * @param theDate Date - when was the item bought?
 * @param sendAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param sendCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param recvAccId String - the UUID associated with a particular
 *            user/player - email, number
 * @param recvCharId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theStoreId long - previously initialized app unique id
 * @param theItemId long - previously initialized app unique id
 * @param theItemPrice float
 * @param theCurrencyType Integer - 0 = real world, 1 = virtual
 * @param theVirtualCurrencyLbl String - a virtual currency previously
 *            initialized from (DIM) Event #14. OPTIONAL: "" if not used.
 * @param theCurrencyValue float - how much of a particular currency was
 *            used for this transaction
 */
- (void) storeBuyItem: (NSDate *) theDate
            sendAccId: (NSString *) sendAccId
           sendCharId: (NSString *) sendCharId
            recvAccId: (NSString *) recvAccId
           recvCharId: (NSString *) recvCharId
           theShardId: (long) theShardId
           theStoreId: (long) theStoreId
            theItemId: (long) theItemId
          theItemName: (NSString *) theItemName
         theItemPrice: (float) theItemPrice
      theCurrencyType: (int) theCurrencyType
theVirtualCurrencyLbl: (NSString *) theVirtualCurrencyLbl
     theCurrencyValue: (float) theCurrencyValue
theVirtualCurencyCount: (int) theVirtualCurrencyCount{
    
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cSTORE_BUY_ITEM] forKey:kTYPE];
    [theEventData setObject:sendAccId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:sendCharId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:recvAccId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:recvCharId forKey:kRECEIVER_CHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theStoreId] forKey:kSTORE_ID];
    [theEventData setObject:[NSNumber numberWithLong:theItemId] forKey:kITEM_ID];
    [theEventData setObject:theItemName forKey:kITEM_NAME];
    [theEventData setObject:[NSNumber numberWithFloat:theItemPrice] forKey:kITEM_PRICE];
    [theEventData setObject:[NSNumber numberWithInt:theCurrencyType] forKey:kCURRENCY_TYPE];
    [theEventData setObject:theVirtualCurrencyLbl forKey:kCURRENCY_VIRTUAL_LABEL];
    [theEventData setObject:[NSNumber numberWithFloat:theCurrencyValue] forKey:kCURRENCY_VALUE];
    [theEventData setObject:[NSNumber numberWithInteger:theVirtualCurrencyCount] forKey:kCURRENCY_VIRTUAL_COUNT];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
    
}

/**
 * log a store login occurence
 *
 * @param theDate Date - when did the user log into the store?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theStoreId long - previously initialized app unique id
 */
- (void) storeLogin: (NSDate *) theDate
       theAccountId: (NSString *) theAccountId
     theCharacterId: (NSString *) theCharacterId
         theShardId: (long) theShardId
         theStoreId: (long) theStoreId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cSTORE_LOGIN] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theStoreId] forKey:kSTORE_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}

/**
 * log a store logout occurence
 *
 * @param theDate Date - when did the logout happen?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theStoreId long - previously initialized app unique id
 */
- (void) storeLogout: (NSDate *) theDate
        theAccountId: (NSString *) theAccountId
      theCharacterId: (NSString *) theCharacterId
          theShardId: (long) theShardId
          theStoreId: (long) theStoreId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cSTORE_LOGOUT] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theStoreId] forKey:kSTORE_ID];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}
/**
 * log an add to cart event
 *
 * @param theDate Date - when was the item added to the "cart"?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theStoreId long
 * @param theItemId long
 * @param theItemName String
 * @param theItemType String
 */
- (void) storeAddToCart: (NSDate *) theDate
           theAccountId: (NSString *) theAccountId
         theCharacterId: (NSString *) theCharacterId
             theStoreId: (long) theStoreId
              theItemId: (long) theItemId
            theItemName:(NSString *) theItemName
            theItemType:(NSString *) theItemType{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cSTORE_CART_ADD] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theStoreId] forKey:kSTORE_ID];
    [theEventData setObject:[NSNumber numberWithLong:theItemId] forKey:kITEM_ID];
    [theEventData setObject:theItemName forKey:kITEM_NAME];
    [theEventData setObject:theItemType forKey:kITEM_TYPE];
    
    [theEvent setObject:theEventData forKey:@"data"];
    [self logEvent:theEvent];
    
}

#pragma mark - Item methods


/**
 * load an Item definition. this must be used before referencing an item id
 * in any other command.
 *
 * @param theDate Date - when was the item created?
 * @param theItemId long - the item's application unique id
 * @param theItemName String - examples: "the ban hammer", "FISH", "kiTTeh"
 * @param theItemType String - examples: "raw materials", "Armor", "tools"
 * @param theItemValue float - the price/numeric value of the item
 * @param theItemSegment String - may be "UGC" or "INGAME"
 * @param createAccountId String - if theItemSegment = "UGC", set this must
 * @param createCharacterId String - if a particular character created the
 *            item. OPTIONAL: "" when not used.
 */
- (void) loadItem:(NSDate *) theDate
        theItemId:(long)theItemId
      theItemName:(NSString *) theItemName
      theItemType:(NSString *) theItemType
   theItemSegment:(NSString *) theItemSegment
  createAccountId:(NSString *) createAccountId
createCharacterId:(NSString *) createCharacterId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cITEM_INIT] forKey:kTYPE];
    [theEventData setObject:[NSNumber numberWithLong:theItemId] forKey:kITEM_ID];
    
    [theEventData setObject:theItemName forKey:kITEM_NAME];
    [theEventData setObject:theItemType forKey:kITEM_TYPE];
    [theEventData setObject:theItemSegment forKey:kITEM_SEGMENT];
    [theEventData setObject:createAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:createCharacterId forKey:kCHARACTER_ID];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
    
}
/**
 * log a user using a certain item
 *
 * @param theDate Date - when was the item used?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLvl long - the numeric level identifier the character
 *            currently is at. OPTIONAL: insert 0 if unused.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theItemId long - application unqiue item id
 * @param theItemCount Integer - number of times the item is used in this
 *            particular location. useful for spells, bullets.
 * @param theAreaId long
 * @param theX float
 * @param theY float
 * @param theZ float
 */
- (void) logItem:(NSDate *)theDate
    theAccountId:(NSString *)theAccountId
  theCharacterId:(NSString *)theCharacterId
 theCharacterLvl:(long)theCharacterLvl
      theShardId:(long)theShardId
       theItemId:(long)theItemId
     theItemName:(NSString *)theItemName
    theItemCount:(int)theItemCount
       theAreaId:(long)theAreaId
     theAreaName:(NSString *)theAreaName
            theX:(float)theX
            theY:(float)theY
            theZ:(float)theZ{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cITEM_USED] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theCharacterLvl] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theItemId] forKey:kITEM_ID];
    [theEventData setObject:theItemName forKey:kITEM_NAME];
    [theEventData setObject:[NSNumber numberWithInt:theItemCount] forKey:kITEM_COUNT];
    [theEventData setObject:[NSNumber numberWithLong:theAreaId] forKey:kAREA_ID];
    [theEventData setObject:theAreaName forKey:kAREA_NAME];
    [theEventData setObject:[NSNumber numberWithFloat:theX] forKey:kPOSITION_X];
    [theEventData setObject:[NSNumber numberWithFloat:theY] forKey:kPOSITION_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theZ] forKey:kPOSITION_Z];
    
    
    
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
    
}
/**
 * log a purchase/sale of an item
 * multiple logItemTransaction events might need to be called for a
 * single transaction if multiple currencies are used.
 * example: 4 gold, 5 silver, 3 copper for a fish trap.
 *
 * @param theDate Date - when did the transaction occur?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theItemId long
 * @param theItemName String
 * @param theItemPrice float
 * @param theTransactionType Integer - 0 = SPENT or 1 = ACQUIRED currency
 * @param theCurrencyType Integer - 0 = real world, 1 = virtual
 * @param theVirtualCurrencyLbl String - a virtual currency previously
 *            initialized from (DIM) Event #14. OPTIONAL: "" if not used.
 * @param theCurrencyValue float - how much of a particular currency was
 *            used for this transaction
 */
-(void) logItemTransaction: (NSDate *) theDate
              theAccountId:(NSString *) theAccountId
            theCharacterId: (NSString *) theCharacterId
                theShardId: (long) theShardId
                 theItemId:(long) theItemId
               theItemName:(NSString *) theItemName
              theItemPrice:(float) theItemPrice
        theTransactionType:(int) theTransactionType
           theCurrencyType:(int) theCurrencyType
     theVirtualCurrencyLbl:(NSString *) theVirtualCurrencyLbl
          theCurrencyValue:(float) theCurrencyValue
    theVitualCurrencyCount:(int) theVirtualCurrencyCount{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cITEM_TRANSACTION] forKey:kTYPE];
    [theEventData setObject:[NSNumber numberWithLong:theItemId] forKey:kITEM_ID];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theItemName forKey:kITEM_NAME];
    [theEventData setObject:[NSNumber numberWithFloat:theItemPrice] forKey:kITEM_PRICE];
    [theEventData setObject:[NSNumber numberWithInt:theTransactionType] forKey:kTRANSACTION_TYPE];
    [theEventData setObject:[NSNumber numberWithInt:theCurrencyType] forKey:kCURRENCY_TYPE];
    [theEventData setObject:theVirtualCurrencyLbl forKey:kCURRENCY_VIRTUAL_LABEL];
    [theEventData setObject:[NSNumber numberWithFloat:theCurrencyValue] forKey:kCURRENCY_VALUE];
    [theEventData setObject:[NSNumber numberWithInt:theVirtualCurrencyCount] forKey:kCURRENCY_VIRTUAL_COUNT];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}


#pragma mark - Shard methods


/**
 * load Shard creation information. this must occur before any logger or
 * load command references the Shard Id.
 *
 * @param theDate Date - when was the Shard created?
 * @param theShardId long - numeric UUID of the Shard
 * @param theShardDesc String - text description of the Shard
 */
- (void) loadShardCreate: (NSDate *) theDate
              theShardId: (long) theShardId
            theShardDesc: (NSString *) theShardDesc{
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cSHARD_INIT] forKey:kTYPE];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theShardDesc forKey:kSHARD_DESCRIPTION];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

#pragma mark - Currency methods
/**
 * initialize a virtual currency for application reporting. this can be
 * used to create a virtual currency and update an exchange rate of an
 * existing currency.
 *
 * @param theCurrencyLabel String - app unique string identifier
 * @param theCurrencyExRate String - multiplicative factor for use in
 *            conversion from virtual to real currency
 */
- (void) loadVirtualCurrency: (NSDate *) theDate
            theCurrencyLabel:(NSString *) theCurrencyLabel{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cVIRTUAL_CURRENCY_SET] forKey:kTYPE];
    [theEventData setObject:theCurrencyLabel forKey:kCURRENCY_VIRTUAL_LABEL];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
}

/**
 * log a transaction of currency
 *
 * @param theDate Date - when did the transaction happen?
 * @param theAccountId String
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theTransactionType Integer - 0 = SPENT or 1 = ACQUIRED currency
 * @param theCurrencyType Integer - 0 = real world, 1 = virtual
 * @param theVirtualCurrencyLbl String - a virtual currency previously
 *            initialized from (DIM) Event #14. OPTIONAL: "" if not used.
 * @param theCurrencyValue float - how much of a particular currency was
 *            used for this transaction
 * @param theTransactionDesc String - max 200 chars OPTIONAL: "" if not used
 */
- (void) logCurrencyTransaction: (NSDate *) theDate
                   theAccountId: (NSString *) theAccountId
                 theCharacterId: (NSString *) theCharacterId
                     theShardId: (long) theShardId
             theTransactionType: (int) theTransactionType
                theCurrencyType: (int) theCurrencyType
          theVirtualCurrencyLbl: (NSString *) theVirtualCurrencyLbl
               theCurrencyValue: (float) theCurrencyValue
             theTransactionDesc: (NSString *) theTransactionDesc
        theVirtualCurrencyCount:(int)theVirtualCurrencyCount{
    
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCURRENCY_TRANSACTION] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithInt:theTransactionType] forKey:kTRANSACTION_TYPE];
    [theEventData setObject:[NSNumber numberWithInt:theCurrencyType] forKey:kCURRENCY_TYPE];
    [theEventData setObject:theVirtualCurrencyLbl forKey:kCURRENCY_VIRTUAL_LABEL];
    [theEventData setObject:[NSNumber numberWithFloat:theCurrencyValue] forKey:kCURRENCY_VALUE];
    [theEventData setObject:theTransactionDesc forKey:kTRANSACTION_DESCRIPTION];
    [theEventData setObject:[NSNumber numberWithInt:theVirtualCurrencyCount]forKey:kCURRENCY_VIRTUAL_COUNT];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}

#pragma mark - UGC methods

/**
 * load a rating for some User Generated Content (UGC)
 *
 * @param theDate Date - when did the rating occur?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theItemId long - the application unique numeric id
 * @param theItemName String - example: "crafty button", "GMO grain"
 * @param theUgcRating VOID - score given to UGC item by other players.
 *            player may rate any item, owned or not. similar to FB like.
 */

- (void) loadUgcRating: (NSDate *) theDate
          theAccountId: (NSString *) theAccountId
        theCharacterId: (NSString *) theCharacterId
            theShardId: (long) theShardId
             theItemId: (long) theItemId
           theItemName: (NSString *) theItemName
          theUgcRating: (NSString *) theUgcRating{
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cITEM_INIT_UGC_RATING] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theItemId] forKey:kITEM_ID];
    [theEventData setObject:theItemName forKey:kITEM_NAME];
    [theEventData setObject:theUgcRating forKey:kRATING_USER_GENERATED_CONTENT];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}
/**
 * track when User Generated Content is copied to another account
 *
 * @param theDate Date - when did the copy occur?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theItemId long - the application unique numeric id
 * @param theItemName String - example: "crafty button", "GMO grain"
 */
-(void) trackUgcCopy: (NSDate *) theDate
        theAccountId: (NSString *) theAccountId
      theCharacterId: (NSString *) theCharacterId
          theShardId: (long) theShardId
           theItemId: (long) theItemId
         theItemName:(NSString *) theItemName{
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cUGC_COPY] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theItemId] forKey:kITEM_ID];
    [theEventData setObject:theItemName forKey:kITEM_NAME];
    
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
    
}

#pragma mark - Group methods

/**
 * log a Group entry event
 *
 * @param theDate Date - when did the user/player enter the Group?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLvl long - the numeric level identifier the character
 *            currently is at. OPTIONAL: insert 0 if unused.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theGroupId long - the id number of the group that was entered
 */
- (void) logEnterGroup: (NSDate *) theDate
          theAccountId: (NSString *) theAccountId
        theCharacterId: (NSString *) theCharacterId
       theCharacterLvl: (long) theCharacterLvl
            theShardId: (long) theShardId
            theGroupId: (long) theGroupId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cGROUP_ENTER] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theCharacterLvl] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theGroupId] forKey:kGROUP_ID];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * log a Group exit event
 *
 * @param theDate Date - when did the user/player exit the Group?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCharacterLvl long - the numeric level identifier the character
 *            currently is at. OPTIONAL: insert 0 if unused.
 * @param theShardId long - a particular db the Character is associated
 *            with. OPTIONAL: insert 0 if unused.
 * @param theGroupId long - the id number of the group that was left
 */
- (void) logExitGroup: (NSDate *) theDate
         theAccountId: (NSString *) theAccountId
       theCharacterId: (NSString *) theCharacterId
      theCharacterLvl: (long) theCharacterLvl
           theShardId: (long) theShardId
           theGroupId: (long) theGroupId{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cGROUP_EXIT] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theCharacterLvl] forKey:kCHARACTER_LEVEL];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theGroupId] forKey:kGROUP_ID];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

#pragma mark - Custom methods

/**
 * log a Custom Slot event
 *
 * @param theDate Date - when did the event was logged?
 * @param theAccountId String - the UUID associated with a particular
 *            user/player - email, number
 * @param theCharacterId String - does an account have multiple characters
 *            associated with it? use this field to track such identifiers.
 *            OPTIONAL: "" when not used.
 * @param theCustomAction - Custom action to be logged
 * @param  theCustomActionType - type of custom action
 * @param  theCustomActionValue - value of custom action
 */
- (void) logCustomAction: (NSDate *) theDate
        theAccountId: (NSString *) theAccountId
        theCharacterId: (NSString *) theCharacterId
        theCustomAction: (NSString *) theCustomAction
        theCustomActionType: (NSString *) theCustomActionType
        theCustomActionValue: (int) theCustomActionValue{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCUSTOM_SLOT] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:theCustomAction forKey:kCUSTOM_ACTION];
    [theEventData setObject:theCustomActionType forKey:kCUSTOM_ACTION_TYPE];
    [theEventData setObject:[NSNumber numberWithInt:theCustomActionValue] forKey:kCUSTOM_ACTION_VALUE];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * Logs FARMER event- Indicates whether the player is a gold farmer and if so what type of gold farmer is he
 *
 * @param theEventTs Date - when did the event occurred
 * @param theAccountId String (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theCharacterId String (MANDATORY Field) - does an account have multiple characters associated with it?
 * 										Use this field to track such identifiers.
 *            							Default - AccountId should be used as default if there's no CharacterId
 * @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 * @param theFarmerType String (MANDATORY Field) - some examples are:Banker, Gatherer, Mule, Spammer
 */
- (void) logFarmer: (NSDate *) theDate
      theAccountId: (NSString *) theAccountId
      theCharacterId: (NSString *) theCharacterId
      theShardId: (long) theShardId
      theFarmerType: (NSString *) theFarmerType{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:[NSDate date]] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cFARMER] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    
    [theEventData setObject:theFarmerType forKey:kFARMER_TYPE];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * Logs a INTEGRITY event- Indiates whether this account was compromised and if so when was this account
 * 							compromised and the type of integrity violation that resulted in compromising this account
 *
 * @param theEventTs Date - when did the event occurred
 * @param theAccountId String (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 * @param theIntegrityType String (MANDATORY Field) - possible examples are: Credit card fraud, cheating etc.
 */

- (void) logIntegrity: (NSDate *)theDate
         theAccountId:(NSString *) theAccountId
           theShardId:(long) theShardId
     theIntegrityType: (NSString *) theIntegrityType{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:[NSDate date]] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cINTEGRITY] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theIntegrityType forKey:kINTEGRITY_TYPE];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
}

/**
 * logs a Reward event - relates to where a player may be awarded a certain reward for
 * 						completeing a certain task or a reward given based on seniority or some other reason.
 *
 * @param theEventTs Date - when did the copy occur?
 * @param theAccountId (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theCharacterId String (MANDATORY Field) - does an account have multiple characters associated with it?
 * 										Use this field to track such identifiers.
 *            							Default - AccountId should be used as default if there's no CharacterId
 * @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 * @param theRewardType Long (MANDATORY Field) - acceptable values are: 0 = Real Currency, 1 = Virtual Currency, 2 = Item
 * @param theRewardId String (MANDATORY Field) - acceptable values are: "1", when RewardType = 0(Real Currency),
 * 												"virtual item name", when RewardType = 1(Virtual Currency),
 * 												"item name", when RewardType = 2(item) - MANDATORY Field
 * @param theItemType Long  (OPTIONAL Field)
 * @param theRewardCount Float (MANDATORY Field) - Real money value, when Reward Type = 0
 * 												count of the virtual currency, when Reward Type = 1
 * 												Item Count, when Reward Type = 2
 */
- (void) logReward: (NSDate *) theDate
   theAccountId: (NSString *) theAccountId
 theCharacterId: (NSString *) theCharacterId
     theShardId: (long) theShardId
  theRewardType: (long) theRewardType
    theRewardId: (NSString *) theRewardId
    theItemType: (long) theItemType
 theRewardCount: (float) theRewardCount{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cREWARD] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theRewardType] forKey:kREWARD_TYPE];
    [theEventData setObject:theRewardId forKey:kREWARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theItemType] forKey:kITEM_TYPE];
    [theEventData setObject:[NSNumber numberWithFloat:theRewardCount] forKey:kREWARD_COUNT];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    

}

/**
 * logs a Area DIM event - Some games have maps associated with them.
 * 						This event saves the dimension information about these maps.
 *
 * @param theEventTs Date - when did the event occur?
 * @param theAreaName String (MANDATORY Field) - It is necessary to pass either AreaId or AreaName
 *  									Default - Empty/Blank "" if does not makes sense/already have passed AreaId
 * @param theAreaId Long (MANDATORY Field) - It is necessary to pass either AreaId or AreaName
 * 										Default - Empty/Blank "" if does not makes sense/already have passed
 * 										AreaName
 * @param theMapId Long (MANDATORY Field) - It is necessary to pass either MapId or MapLabel
 * 										Default - Empty/Blank "" if does not makes sense/already have passed
 * 									    MapLabel
 * @param theMapLabel String (MANDATORY Field) - It is necessary to pass either MapId or MapLabel
 *  									Default - Empty/Blank "" if does not makes sense/already have passed MapId
 * @param theMinX Float (MANDATORY Field)
 * @param theMinY Float (MANDATORY Field)
 * @param theMinZ Float (OPTIONAL Field)
 * @param theMaxX Float  (MANDATORY Field)
 * @param theMaxY Float (MANDATORY Field)
 * @param theMaxZ Float (OPTIONAL Field)
 */

- (void) logAreaDim: (NSDate *) theDate
     theAreaName:    (NSString *) theAreaName
       theAreaId:    (long)  theAreaId
        theMapId:    (long) theMapId
     theMapLabel:    (NSString *) theMapLabel
         theMinX:    (float) theMinX
         theMinY:    (float) theMinY
         theMinZ:    (float) theMinZ
         theMaxX:    (float) theMaxX
         theMaxY:    (float) theMaxY
         theMaxZ:    (float) theMaxZ{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cAREA_DIM] forKey:kTYPE];
    [theEventData setObject:theAreaName forKey:kAREA_NAME];
    [theEventData setObject:[NSNumber numberWithLong:theAreaId] forKey:kAREA_ID];
    [theEventData setObject:[NSNumber numberWithLong:theMapId] forKey:kMAP_ID];
    [theEventData setObject:theMapLabel forKey:kMAP_LABEL];
    [theEventData setObject:[NSNumber numberWithFloat:theMinX] forKey:kMIN_X];
    [theEventData setObject:[NSNumber numberWithFloat:theMinY] forKey:kMIN_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theMinZ] forKey:kMIN_Z];
    [theEventData setObject:[NSNumber numberWithFloat:theMaxX] forKey:kMAX_X];
    [theEventData setObject:[NSNumber numberWithFloat:theMaxY] forKey:kMAX_Y];
    [theEventData setObject:[NSNumber numberWithFloat:theMaxZ] forKey:kMAX_Z];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * Logs a MOB event - Mobs refers to groupings of non-player characters (NPCs). This event captures the cases
 * 					when the player confronts a mobs and kills that mob. This event captures the cases when the
 * 					player confronts a mob and kills that mob
 *
 * @param theEventTs Date - when did the event occur?
 * @param theAccountId String(MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theCharacterId String (MANDATORY Field) - does an account have multiple characters associated with it?
 * 										Use this field to track such identifiers.
 *										Default - AccountId should be used as default if there's no CharacterId
 * @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 *  @param theMobDesc String (OPTIONAL Field)
 */
- (void) logMob: (NSDate *) theDate
theAccountId:    (NSString *) theAccountId
theCharacterId:    (NSString *) theCharacterId
  theShardId:    (long)  theShardId
theKillMobDesc: (NSString *) theKillMobDesc{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cMOB] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theKillMobDesc forKey:kMOB_DESC];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
}

/**
 * Logs a CRM Action event-  This is generic field for CRM (Customer Relationshop management) action that may be taken
 * 							by a CRM person in the gaming company. Information regarding how the customer responded to
 * 							the action is also saved
 *
 * @param theEventTs Date - when did the event occur?
 * @param theAccountId String (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theCharacterId String (MANDATORY Field) - does an account have multiple characters associated with it?
 * 										Use this field to track such identifiers.
 *										Default - AccountId should be used as default if there's no CharacterId
 * @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 *  @param theCrmAction String (MANDATORY Field)
 *  @param theCrmActionType String (MANDATORY Field)
 *  @param theCrmFulfilledStatus String (OPTIONAL Field)
 *  @param theCrmFulfilledTs Date (OPTIONAL Field) - when does the CRM action fulfilled?
 */
- (void) logCRMAction: (NSDate *)    theDate
      theAccountId: (NSString *) theAccountId
    theCharacterId: (NSString *) theCharacterId
        theShardId: (long)    theShardId
      theCrmAction: (NSString *) theCrmAction
  theCrmActionType: (NSString *) theCrmActionType
theCrmFulfilledStatus: (NSString *) theCrmFulfilledStatus
 theCrmFulfilledTs: (NSDate *) theCrmFulfilledTs{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCRM_ACTION] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theCrmAction forKey:kCRM_ACTION];
    [theEventData setObject:theCrmActionType forKey:kCRM_ACTION_TYPE];
    [theEventData setObject:theCrmFulfilledStatus forKey:kCRM_FULFILLED_STATUS];
    [theEventData setObject:[self dateToJSON:theCrmFulfilledTs] forKey:kCRM_FULFILLED_TIMESTAMP];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    [self logEvent:theEvent];
    
}

/**
 * logs a Customer Service Action event -  This event captures all the events which are not described in any of the other
 * 									events but still events which are within the game.
 *
 * @param theEventTs Date - when did the event occur?
 * @param theAccountId String (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theCharacterId String (MANDATORY Field) - does an account have multiple characters associated with it?
 * 										Use this field to track such identifiers.
 *										Default - AccountId should be used as default if there's no CharacterId
 * @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 *  @param theCsAction String (MANDATORY Field)
 *  @param theCsActionType String (MANDATORY Field)
 */
- (void) logCustomerServiceAction: (NSDate *) theDate
             theAccountId: (NSString *) theAccountId
           theCharacterId: (NSString *) theCharacterId
               theShardId: (long)   theShardId
              theCsAction: (NSString *) theCsAction
          theCsActionType: (NSString *) theCsActionType{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cCUSTOMER_SERVICE_ACTION] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theCsAction forKey:kCUSTOMER_SERVICE_ACTION];
    [theEventData setObject:theCsActionType forKey:kCUSTOMER_SERVICE_ACTION_TYPE];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * logs a Productivity event -  This event refers to a scenario where any time a player crafts, harvests
 * 								etc an item within the game.
 *
 * @param theEventTs Date - when did the event occur?
 * @param theAccountId String (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theCharacterId String (MANDATORY Field) - does an account have multiple characters associated with it?
 * 										Use this field to track such identifiers.
 *										Default - AccountId should be used as default if there's no CharacterId
 * @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 *  @param theProductivityType String (MANDATORY Field)
 */

- (void)    logProductivity: (NSDate *) theDate
            theAccountId: (NSString *) theAccountId
          thecharacterId: (NSString *) theCharacterId
              theShardId: (long)    theShardId
     theProductivityType: (NSString *) theProductivityType{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cPRODUCTIVITY] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theProductivityType forKey:kPRODUCTIVITY_TYPE];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    [self logEvent:theEvent];
    
}

/**
 * logs a Resource event -  Refers to events where any time a player acquires a non-item resource,
 * 							e.g. coins, gold, power, etc.
 *
 * @param theEventTs Date - when did the event occur?
 * @param theAccountId String (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theCharacterId String (MANDATORY Field) - does an account have multiple characters associated with it?
 * 										Use this field to track such identifiers.
 *										Default - AccountId should be used as default if there's no CharacterId
 * @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 *  @param theResourceType String (MANDATORY Field) - acceptable values are: 0 = Real Currency, 1 = Virtual Currency, 2 = item
 *  @param theResourceId String (MANDATORY Field) - acceptable values are: "1", when ResourceType = 0(Real Currency),
 * 											"virtual item name", when ResourceType = 1(Virtual Currency),
 * 											"item name", when ResourceType = 2(item)
 *  @param theResourceCount Long (MANDATORY Field) - Real money value, when ResourceType = 0
 * 											count of the virtual currency, when ResourceType = 1
 * 											Item Count, when ResourceType = 2
 */
- (void) logResource: (NSDate *) theDate
     theAccountId: (NSString *) theAccountId
   thecharacterId: (NSString *) theCharacterId
       theShardId: (long)    theShardId
  theResourceType: (NSString *) theResourceType
    theResourceId: (NSString *) theResourceId
 theResourceCount: (long)   theResourceCount{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];

    [theEventData setObject:[NSNumber numberWithInt:cRESOURCE] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theCharacterId forKey:kCHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theResourceType forKey:kRESOURCE_TYPE];
    [theEventData setObject:theResourceId forKey:kRESOURCE_ID];
    [theEventData setObject:[NSNumber numberWithLong:theResourceCount] forKey:kRESOURCE_COUNT];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * Logs a Ad Click event -  This event is a record of the user who either clicks an ad or had already clicked
 * 								the ad and then taking some subsequent action.
 *
 * @param theEventTs Date - when did the event occur?
 * @param theAccountId String (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 * @param theAdTagName String (OPTIONAL Field) - Possible values - In-App,Google, Facebook, Ad-Words, Other, or free text, Ad4Game,
 Addictive Mobility, Adelphic, Adknowledge, AdTheorent, Altrooz, AppFlood, Appia, AppLift, AppLovin, AppNexus, AppTap,
 Aquto (MoVE), Clickky, CrowdMob, Drawbridge, Eclipse-io, Geenapp, Greystripe, HeyZap, Iconpeak, InMobi, Jampp, JumpTap,
 Kickbit, LiveIntent, madnet, Manage.com, MdotM, Millennial, mNectar, Mobile Theory, Moblin, Moboqo, mobpartner, Motive Interactive,
 motrixi, MyLikes, Nexage, Pandora, RadiumOne, Rocket Fuel, Rovio, SessionM, SponsorPay, StrikeAd, Surikate, TapIt, Taptica, TrialPay,
 Velti, Verve, Voltari, YieldMo
 Default - In-App
 * @param theAdActionTs Date (OPTIONAL Field)
 */

- (void) logAccountAd: (NSDate *) theDate
    theAccountId: (NSString *) theAccountId
    theAdTagName: (NSString *) theAdTagName
   theAdActionTs: (NSDate *) theAdActionTs{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];

    [theEventData setObject:[NSNumber numberWithInt:cAD_CLICK] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:theAdTagName forKey:kAD_TAG_NAME];
    [theEventData setObject:[self dateToJSON:theAdActionTs] forKey:kAD_ACTION_TIMESTAMP];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

/**
 * logs a Economic event -  This item refers TO Economic Type: Buy item, Sell item, Trade away item
 Trade acquire item, Create item
 *
 * @param theEventTs Date - when did the event occur?
 * @param sendAccId Long (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 *  @param sendCharId Long (MANDATORY Field) - does an account have multiple characters associated with it?
 * 							Use this field to track such identifiers.
 *            				Default - sendAccId should be used as default if there's no sendCharId
 *  @param recvAccId Long (MANDATORY Field) - the UUID associated with a particular user/player - email, number
 *  @param recvCharId Long (MANDATORY Field) - does an account have multiple characters associated with it?
 * 							Use this field to track such identifiers.
 *            				Default - recvAccId should be used as default if there's no recvCharId
 *  @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
 * 										Default - Use 0 if unused or does not have a value
 *  @param theItemId Long (MANDATORY Field) - the item's application unique id
 * 										Default - Use 0 for default value
 *  @param theItemName String (MANDATORY Field) - examples: "the ban hammer", "FISH", "kiTTeh"
 *  @param theEconomicType String (MANDATORY Field) - some example types are:  Buy item, Sell item, Trade away item, Trade acquire item,
 Create item
 *  @param theEconomicValue Float (MANDATORY Field)
 *  @param theCurrencyType Integer (MANDATORY Field) - acceptable values are: 0 = real world, 1 = virtual
 *  @param theVirtualCurrencyLbl String String (OPTIONAL Field) - a virtual currency previously initialized from Event #14.
 * 										If CurrencyType = 1, set this Mandatory, else send " ".
 *  @param theCurrencyValue Float (MANDATORY Field) - how much of a particular currency was used for this transaction
 *
 */

- (void) logEconomic: (NSDate *) theDate
theSenderAccountId: (NSString *) theSenderAccountId
theSenderCharacterId: (NSString *) theSenderCharacterId
theReceiverAccountId: (NSString *) theReceiverAccountId
theReceiverCharacterId: (NSString *) theReceiverCharacterId
       theShardId: (long)  theShardId
        theItemId: (long) theItemId
      theItemName: (NSString *) theItemName
  theEconomicType: (NSString *) theEconomicType
 theEconomicValue: (float) theEconomicValue
  theCurrencyType: (NSString *) theCurrencyType
theVirtualCurrencyType: (NSString *) theVirtualCurrencyType
 theCurrencyValue: (float) theCurrencyValue
theVirtualCurrencyCount: (int ) theVirtualCurrencyCount{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cECONOMIC] forKey:kTYPE];
    [theEventData setObject:theSenderAccountId forKey:kSENDER_ACCOUNT_ID];
    [theEventData setObject:theSenderCharacterId forKey:kSENDER_CHARACTER_ID];
    [theEventData setObject:theReceiverAccountId forKey:kRECEIVER_ACCOUNT_ID];
    [theEventData setObject:theReceiverCharacterId forKey:kRECEIVER_CHARACTER_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:[NSNumber numberWithLong:theItemId] forKey:kITEM_ID];
    [theEventData setObject:theItemName forKey:kITEM_NAME];
    [theEventData setObject:theEconomicType forKey:kECONOMIC_TYPE];
    [theEventData setObject:[NSNumber numberWithFloat:theEconomicValue] forKey:kECONOMIC_VALUE];
    [theEventData setObject:theCurrencyType forKey:kCURRENCY_TYPE];
    [theEventData setObject:theVirtualCurrencyType forKey:kCURRENCY_VIRTUAL_LABEL];
    [theEventData setObject:[NSNumber numberWithFloat:theCurrencyValue] forKey:kCURRENCY_VALUE];
    [theEventData setObject:[NSNumber numberWithInt:theVirtualCurrencyCount] forKey:kCURRENCY_VIRTUAL_COUNT];
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    [self logEvent:theEvent];
    
}

/**
* Logs a Traffic Source event -  .
*
* @param theDate Date - when did the event occur?
* @param theAccountId String (MANDATORY Field) - the UUID associated with a particular user/player - email, number
* @param theTrafficSource String (MANDATORY Field)
* @param theShardId Long (OPTIONAL Field)- a particular db the Character is associated with
* 										Default - Use 0 if unused or does not have a value
* @param theTrafficSourceType String (MANDATORY Field)
*/

- (void) logTrafficSource: (NSDate *) theDate
          theAccountId: (NSString *) theAccountId
      theTrafficSource: (NSString *) theTrafficSource
            theShardId: (long) theShardId
  theTrafficSourceType: (NSString *) theTrafficSoureType{
    
    NSMutableDictionary *theEvent = [[NSMutableDictionary alloc] init];
    [theEvent setObject:[self dateToJSON:theDate] forKey:@"timestamp"];
    
    NSMutableDictionary *theEventData = [[NSMutableDictionary alloc] init];
    
    [theEventData setObject:[NSNumber numberWithInt:cTRAFFIC_SOURCE] forKey:kTYPE];
    [theEventData setObject:theAccountId forKey:kACCOUNT_ID];
    [theEventData setObject:[NSNumber numberWithLong:theShardId] forKey:kSHARD_ID];
    [theEventData setObject:theTrafficSoureType forKey:kTRAFFIC_SOURCE_TYPE];
    [theEventData setObject:theTrafficSource forKey:kTRAFFIC_SOURCE];
    
    
    [theEvent setObject:theEventData forKey:@"data"];
    
    
    [self logEvent:theEvent];
    
}

@end
